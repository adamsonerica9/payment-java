import me.qoomon.gradle.gitversioning.GitVersioningPluginConfig
import me.qoomon.gradle.gitversioning.GitVersioningPluginConfig.*

plugins {
    id("me.qoomon.git-versioning") version "4.3.0"
    id("io.github.gradle-nexus.publish-plugin") version "1.3.0"
    id("com.github.ben-manes.versions") version "0.48.0"
    id("se.patrikerdes.use-latest-versions") version "0.2.18" // gradle useLatestVersions
}

tasks.register("printVersion") {
    group = "Development"
    doLast {
        println(project.version)
    }
}

gitVersioning.apply(closureOf<GitVersioningPluginConfig> {
    tag(closureOf<VersionDescription>{
        pattern = "v(?<tagVersion>[0-9].*)"
        versionFormat = "\${tagVersion}\${dirty.snapshot}"
    })
    tag(closureOf<VersionDescription>{
        pattern = "snapshot-(?<tagVersion>.+)"
        versionFormat = "\${tagVersion}\${dirty}-SNAPSHOT"
    })
    tag(closureOf<VersionDescription>{
        pattern = ".+"
        versionFormat = "\${tag.slug}-\${commit.short}\${dirty.snapshot}"
    })
    branch(closureOf<VersionDescription>{
        pattern = "master"
        versionFormat = "master-\${commit.short}\${dirty.snapshot}"
    })
    branch(closureOf<VersionDescription>{
        pattern = ".+"
        versionFormat = "\${branch.slug}-\${commit.short}\${dirty.snapshot}"
    })
})

nexusPublishing {
    repositories {
        create("mavenCentral") {
            val ossrhUsername: String? by project
            val ossrhPassword: String? by project
            // see https://status.maven.org/ in case of artifact publishing issues
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(ossrhUsername)
            password.set(ossrhPassword)
        }
    }
}
