package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.AuthTokenOperations
import tech.carpentum.sdk.payment.MerchantInfoApi
import tech.carpentum.sdk.payment.MerchantInfoApi.Factory.defineGetMerchantInfoEndpoint
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.ResponseException
import tech.carpentum.sdk.payment.model.MerchantInfo
import java.io.InterruptedIOException
import java.time.Duration

/**
 * Wraps all MerchantInfo API operations.
 */
class MerchantInfoScenario private constructor(private val merchantInfoApi: MerchantInfoApi, authToken: String) :
    AbstractScenario(authToken) {

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun getMerchantInfo(): MerchantInfo {
        return merchantInfoApi.getMerchantInfo()
    }

    companion object {
        /**
         * Creates new instance of the scenario with newly created Authorization token with specified validity.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, validity: Duration? = null): MerchantInfoScenario {
            val token = context.createAuthToken(
                AuthTokenOperations(defineGetMerchantInfoEndpoint()),
                validity
            ).token
            return MerchantInfoScenario(MerchantInfoApi.create(context, token), token)
        }

        /**
         * Creates new instance of the scenario with existing Authorization token.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, authToken: String): MerchantInfoScenario {
            return MerchantInfoScenario(MerchantInfoApi.create(context, authToken), authToken)
        }
    }
}
