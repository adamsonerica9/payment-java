package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.AuthTokenOperations
import tech.carpentum.sdk.payment.OutgoingPaymentsApi
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.ResponseException
import tech.carpentum.sdk.payment.model.PayoutDetail
import java.io.InterruptedIOException
import java.time.Duration

/**
 * Wraps just GET operation of Outgoing Payments API for single `idPayout`.
 */
class GetPayoutScenario private constructor(
    private val outgoingPaymentsApi: OutgoingPaymentsApi,
    private val idPayout: String,
    authToken: String
) : AbstractScenario(authToken) {

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun getDetail(): PayoutDetail {
        return outgoingPaymentsApi.getPayout(idPayout)
    }

    companion object {
        /**
         * Creates new instance of the scenario with newly created Authorization token with specified validity.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(
            context: PaymentContext,
            idPayout: String,
            validity: Duration? = null
        ): GetPayoutScenario {
            val token = context.createAuthToken(
                AuthTokenOperations(OutgoingPaymentsApi.defineGetPayoutEndpoint().forId(idPayout)),
                validity
            ).token
            return GetPayoutScenario(OutgoingPaymentsApi.create(context, token), idPayout, token)
        }

        /**
         * Creates new instance of the scenario with existing Authorization token.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, idPayout: String, authToken: String): GetPayoutScenario {
            return GetPayoutScenario(OutgoingPaymentsApi.create(context, authToken), idPayout, authToken)
        }
    }
}