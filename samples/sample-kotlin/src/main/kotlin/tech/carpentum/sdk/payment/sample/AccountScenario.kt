package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.AccountsApi
import tech.carpentum.sdk.payment.AccountsApi.Factory.defineListBalancesEndpoint
import tech.carpentum.sdk.payment.AuthTokenOperations
import tech.carpentum.sdk.payment.PaymentContext
import tech.carpentum.sdk.payment.ResponseException
import tech.carpentum.sdk.payment.model.BalanceList
import java.io.InterruptedIOException
import java.time.Duration

/**
 * Wraps all Accounts API operations.
 */
class AccountScenario private constructor(private val accountsApi: AccountsApi, authToken: String) :
    AbstractScenario(authToken) {

    @Throws(ResponseException::class, InterruptedIOException::class)
    fun listBalances(vararg currencyCodes: String): BalanceList {
        return accountsApi.listBalances(setOf(*currencyCodes))
    }

    companion object {
        /**
         * Creates new instance of the scenario with newly created Authorization token with specified validity.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, validity: Duration? = null): AccountScenario {
            val token = context.createAuthToken(
                AuthTokenOperations(defineListBalancesEndpoint()),
                validity
            ).token
            return AccountScenario(AccountsApi.create(context, token), token)
        }

        /**
         * Creates new instance of the scenario with existing Authorization token.
         */
        @Throws(ResponseException::class, InterruptedIOException::class)
        fun create(context: PaymentContext, authToken: String): AccountScenario {
            return AccountScenario(AccountsApi.create(context, authToken), authToken)
        }
    }
}
