package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.model.PayoutDetail
import tech.carpentum.sdk.payment.{AuthTokenOperations, OutgoingPaymentsApi, PaymentContext, ResponseException}

import java.time.Duration

/**
 * Wraps just GET operation of Outgoing Payments API for single `idPayout`.
 */
object GetPayoutScenario {
  /**
   * Creates new instance of the scenario with newly created Authorization token with specified validity.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayout: String, validity: Duration = null): GetPayoutScenario = {
    val authToken = context.createAuthToken(
      new AuthTokenOperations(OutgoingPaymentsApi.defineGetPayoutEndpoint.forId(idPayout)),
      validity
    ).getToken
    new GetPayoutScenario(OutgoingPaymentsApi.create(context, authToken), idPayout, authToken)
  }

  /**
   * Creates new instance of the scenario with existing Authorization token.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayout: String, authToken: String) =
    new GetPayoutScenario(OutgoingPaymentsApi.create(context, authToken), idPayout, authToken)
}

class GetPayoutScenario private(val outgoingPaymentsApi: OutgoingPaymentsApi, val idPayout: String, override val authToken: String) extends AbstractScenario(authToken) {
  @throws[ResponseException]
  def getDetail: PayoutDetail = outgoingPaymentsApi.getPayout(idPayout)
}
