package tech.carpentum.sdk.payment.sample

import org.slf4j.LoggerFactory
import tech.carpentum.sdk.payment.{AccountsApi, AuthTokenOperations, OutgoingPaymentsApi, PaymentContext, ResponseException}
import tech.carpentum.sdk.payment.model.{Payout, PayoutAccepted, PayoutDetail}

import java.time.Duration
import java.util.Optional
import scala.jdk.CollectionConverters.SetHasAsJava

/**
 * A little complex scenario that combines Accounts API and Outgoing Payments API operations
 * with Authorization token for both APIs and single `idPayout`.
 */
object CreatePayoutWithBalanceCheckScenario {
  private val LOGGER = LoggerFactory.getLogger(classOf[CreatePayoutWithBalanceCheckScenario])

  /**
   * Creates new instance of the scenario with newly created Authorization token with specified validity.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayout: String, validity: Duration = null): CreatePayoutWithBalanceCheckScenario = {
    val authToken = context.createAuthToken(
      new AuthTokenOperations()
        .grant(AccountsApi.defineListBalancesEndpoint)
        .grant(OutgoingPaymentsApi.defineCreatePayoutEndpoint.forId(idPayout))
        .grant(OutgoingPaymentsApi.defineGetPayoutEndpoint.forId(idPayout)),
      validity
    ).getToken
    new CreatePayoutWithBalanceCheckScenario(
      AccountsApi.create(context, authToken, Duration.ofSeconds(10)),
      OutgoingPaymentsApi.create(context, authToken, Duration.ofSeconds(20)),
      idPayout,
      authToken
    )
  }

  /**
   * Creates new instance of the scenario with existing Authorization token.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayout: String, authToken: String) =
    new CreatePayoutWithBalanceCheckScenario(
      AccountsApi.create(context, authToken, Duration.ofSeconds(10)),
      OutgoingPaymentsApi.create(context, authToken, Duration.ofSeconds(20)),
      idPayout,
      authToken
    )
}

class CreatePayoutWithBalanceCheckScenario private(val accountsApi: AccountsApi, val outgoingPaymentsApi: OutgoingPaymentsApi, val idPayout: String, override val authToken: String) extends AbstractScenario(authToken) {
  @throws[ResponseException]
  def createPaymentWithBalanceCheck(payout: Payout): Optional[_ <: PayoutAccepted] = {
    val requestedBalance = payout.getPaymentRequested.getMoney.getAmount
    try {
      val balancesResponse = accountsApi.listBalances(Set(payout.getPaymentRequested.getMoney.getCurrencyCode.getValue).asJava)
      val availableBalance = balancesResponse.getData.get(0).getAvailableBalance
      if (availableBalance.compareTo(requestedBalance) > 0) Optional.of(outgoingPaymentsApi.createPayout(idPayout, payout))
      else {
        CreatePayoutWithBalanceCheckScenario.LOGGER.debug("Insufficient balance. Available: {}, Requested: {}.", availableBalance, requestedBalance)
        Optional.empty
      }
    } catch {
      case ex: ResponseException =>
        CreatePayoutWithBalanceCheckScenario.LOGGER.debug("Error response while getting available balance. Detail: {}", ex.getLocalizedMessage)
        throw ex
    }
  }

  @throws[ResponseException]
  def getDetail: PayoutDetail = outgoingPaymentsApi.getPayout(idPayout)
}
