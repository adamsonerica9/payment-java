package tech.carpentum.sdk.payment.sample

import tech.carpentum.sdk.payment.model.PayinDetail
import tech.carpentum.sdk.payment.{AuthTokenOperations, IncomingPaymentsApi, PaymentContext, ResponseException}

import java.time.Duration

/**
 * Wraps just GET operation of Incoming Payments API for single `idPayin`.
 */
object GetPayinScenario {
  /**
   * Creates new instance of the scenario with newly created Authorization token with specified validity.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayin: String, validity: Duration = null): GetPayinScenario = {
    val authToken = context.createAuthToken(
      new AuthTokenOperations(IncomingPaymentsApi.defineGetPayinEndpoint.forId(idPayin)),
      validity
    ).getToken
    new GetPayinScenario(IncomingPaymentsApi.create(context, authToken), idPayin, authToken)
  }

  /**
   * Creates new instance of the scenario with existing Authorization token.
   */
  @throws[ResponseException]
  def create(context: PaymentContext, idPayin: String, authToken: String) =
    new GetPayinScenario(IncomingPaymentsApi.create(context, authToken), idPayin, authToken)
}

class GetPayinScenario private(val incomingPaymentsApi: IncomingPaymentsApi, val idPayin: String, override val authToken: String) extends AbstractScenario(authToken) {
  @throws[ResponseException]
  def getDetail: PayinDetail = incomingPaymentsApi.getPayin(idPayin)
}
