package tech.carpentum.sdk.payment.sample;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.OutgoingPaymentsApi;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.ResponseException;
import tech.carpentum.sdk.payment.model.AuthTokenResponse;
import tech.carpentum.sdk.payment.model.PayoutDetail;

import java.io.InterruptedIOException;
import java.time.Duration;

/**
 * Wraps just GET operation of Outgoing Payments API for single `idPayout`.
 */
public class GetPayoutScenario extends AbstractScenario {
    private final OutgoingPaymentsApi outgoingPaymentsApi;
    private final String idPayout;

    private GetPayoutScenario(@NotNull OutgoingPaymentsApi outgoingPaymentsApi, @NotNull String idPayout, @NotNull String authToken) throws ResponseException {
        super(authToken);
        this.outgoingPaymentsApi = outgoingPaymentsApi;
        this.idPayout = idPayout;
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with specified validity.
     */
    public static GetPayoutScenario create(@NotNull PaymentContext context, @NotNull String idPayout, @Nullable Duration validity)
            throws ResponseException, InterruptedIOException {
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations(OutgoingPaymentsApi.defineGetPayoutEndpoint().forId(idPayout)),
                validity
        );
        return new GetPayoutScenario(OutgoingPaymentsApi.create(context, authToken.getToken()), idPayout, authToken.getToken());
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with default {@code payment} validity.
     */
    public static GetPayoutScenario create(@NotNull PaymentContext context, @NotNull String idPayout) throws ResponseException, InterruptedIOException {
        return create(context, idPayout, (Duration) null);
    }

    /**
     * Creates new instance of the scenario with existing Authorization token.
     */
    public static GetPayoutScenario create(@NotNull PaymentContext context, @NotNull String idPayout, @NotNull String authToken) throws ResponseException {
        return new GetPayoutScenario(OutgoingPaymentsApi.create(context, authToken), idPayout, authToken);
    }

    public PayoutDetail getDetail() throws ResponseException, InterruptedIOException {
        return outgoingPaymentsApi.getPayout(idPayout);
    }
}
