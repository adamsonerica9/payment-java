package tech.carpentum.sdk.payment.sample;

import org.jetbrains.annotations.NotNull;

abstract class AbstractScenario {
    private final String authToken;

    public AbstractScenario(@NotNull String authToken) {
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }
}
