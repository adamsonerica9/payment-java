package tech.carpentum.sdk.payment.sample;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tech.carpentum.sdk.payment.AuthTokenOperations;
import tech.carpentum.sdk.payment.MerchantInfoApi;
import tech.carpentum.sdk.payment.PaymentContext;
import tech.carpentum.sdk.payment.ResponseException;
import tech.carpentum.sdk.payment.model.AuthTokenResponse;
import tech.carpentum.sdk.payment.model.MerchantInfo;

import java.io.InterruptedIOException;
import java.time.Duration;

/**
 * Wraps all MerchantInfo API operations.
 */
public class MerchantInfoScenario extends AbstractScenario {
    private final MerchantInfoApi merchantInfoApi;

    private MerchantInfoScenario(@NotNull MerchantInfoApi merchantInfoApi, @NotNull String authToken) {
        super(authToken);
        this.merchantInfoApi = merchantInfoApi;
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with specified validity.
     */
    public static MerchantInfoScenario create(@NotNull PaymentContext context, @Nullable Duration validity) throws ResponseException, InterruptedIOException {
        AuthTokenResponse authToken = context.createAuthToken(
                new AuthTokenOperations(MerchantInfoApi.defineGetMerchantInfoEndpoint()),
                validity
        );
        return new MerchantInfoScenario(MerchantInfoApi.create(context, authToken.getToken()), authToken.getToken());
    }

    /**
     * Creates new instance of the scenario with newly created Authorization token with default {@code payment} validity.
     */
    public static MerchantInfoScenario create(@NotNull PaymentContext context) throws ResponseException, InterruptedIOException {
        return create(context, (Duration) null);
    }

    /**
     * Creates new instance of the scenario with existing Authorization token.
     */
    public static MerchantInfoScenario create(@NotNull PaymentContext context, @NotNull String authToken) throws ResponseException {
        return new MerchantInfoScenario(MerchantInfoApi.create(context, authToken), authToken);
    }

    public MerchantInfo getMerchantInfo() throws ResponseException, InterruptedIOException {
        return merchantInfoApi.getMerchantInfo();
    }

}
