plugins {
    id("tech.carpentum.sdk.payment.java-application-conventions")
}

dependencies {
    implementation(project(":payment-client-v2"))
    implementation("ch.qos.logback:logback-classic")
    implementation("org.jetbrains:annotations")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}

application {
    // Define the main class for the application.
    mainClass.set("tech.carpentum.sdk.payment.sample.AdvancedScenariosApp")
}
