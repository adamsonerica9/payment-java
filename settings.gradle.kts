rootProject.name = "sdk-payment-java"
include("openapi-generator", "payment-client-v2", "samples:sample-java", "samples:sample-kotlin", "samples:sample-scala")
