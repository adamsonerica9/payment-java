//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** OFFLINE
 *
 * The account parameters for this payment method are used to show payment instructions to the customer.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface OfflineMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.OFFLINE;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull AccountResponseOffline getAccount();

    @NotNull Money getMoney();

    @NotNull Optional<MoneyRequired> getMoneyRequired();

    @NotNull Optional<MoneyVat> getVat();

    /** Reference number of transaction. */
    @NotNull String getReference();

    @NotNull Optional<PaymentProcessor> getProcessor();

    /** The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer. */
    @NotNull Optional<String> getQrName();

    /** The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.

This parameter is returned only for the VND currency. */
    @NotNull Optional<String> getQrCode();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    @NotNull Optional<PaymentOperatorIncoming> getPaymentOperator();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(OfflineMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.account(copyOf.getAccount());
        builder.money(copyOf.getMoney());
        builder.moneyRequired(copyOf.getMoneyRequired().orElse(null));
        builder.vat(copyOf.getVat().orElse(null));
        builder.reference(copyOf.getReference());
        builder.processor(copyOf.getProcessor().orElse(null));
        builder.qrName(copyOf.getQrName().orElse(null));
        builder.qrCode(copyOf.getQrCode().orElse(null));
        builder.returnUrl(copyOf.getReturnUrl());
        builder.paymentOperator(copyOf.getPaymentOperator().orElse(null));
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new OfflineMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link OfflineMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link OfflineMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);


        /**
          * Set {@link OfflineMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);


        /**
          * Set {@link OfflineMethodResponse#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountResponseOffline account);


        /**
          * Set {@link OfflineMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(Money money);


        /**
          * Set {@link OfflineMethodResponse#getMoneyRequired} property.
          *
          * 
          */
        @NotNull Builder moneyRequired(MoneyRequired moneyRequired);


        /**
          * Set {@link OfflineMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);


        /**
          * Set {@link OfflineMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);


        /**
          * Set {@link OfflineMethodResponse#getProcessor} property.
          *
          * 
          */
        @NotNull Builder processor(PaymentProcessor processor);


        /**
          * Set {@link OfflineMethodResponse#getQrName} property.
          *
          * The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @NotNull Builder qrName(String qrName);


        /**
          * Set {@link OfflineMethodResponse#getQrCode} property.
          *
          * The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.

This parameter is returned only for the VND currency.
          */
        @NotNull Builder qrCode(String qrCode);


        /**
          * Set {@link OfflineMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);


        /**
          * Set {@link OfflineMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @NotNull Builder paymentOperator(PaymentOperatorIncoming paymentOperator);


        /**
          * Set {@link OfflineMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);


        /**
          * Set {@link OfflineMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);


        /**
         * Create new instance of {@link OfflineMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull OfflineMethodResponse build();

    }
}