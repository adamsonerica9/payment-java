//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CurrencyCode

class CurrencyCodeJsonAdapter {

    @FromJson
    fun fromJson(json: String): CurrencyCode {
        return CurrencyCode.of(json)
    }

    @ToJson
    fun toJson(model: CurrencyCode): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: CurrencyCode): CurrencyCodeImpl {
        return model as CurrencyCodeImpl
    }

    @ToJson
    fun toJsonImpl(impl: CurrencyCodeImpl): CurrencyCode {
        return impl
    }

}