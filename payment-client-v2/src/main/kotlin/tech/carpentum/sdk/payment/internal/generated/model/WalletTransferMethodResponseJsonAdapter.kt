//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.WalletTransferMethodResponse

class WalletTransferMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: WalletTransferMethodResponseJson): WalletTransferMethodResponse {
        val builder = WalletTransferMethodResponse.builder()
        builder.idPayout(json.idPayout)
        builder.idPayment(json.idPayment)
        builder.reference(json.reference)
        return builder.build()
    }

    @ToJson
    fun toJson(model: WalletTransferMethodResponse): WalletTransferMethodResponseJson {
        val json = WalletTransferMethodResponseJson()
        json.idPayout = model.idPayout
        json.idPayment = model.idPayment
        json.paymentMethodCode = model.paymentMethodCode.name
        json.reference = model.reference
        return json
    }

    @FromJson
    fun fromJsonImpl(model: WalletTransferMethodResponse): WalletTransferMethodResponseImpl {
        return model as WalletTransferMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: WalletTransferMethodResponseImpl): WalletTransferMethodResponse {
        return impl
    }

}