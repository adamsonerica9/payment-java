//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestIMPS

class AccountPayinRequestIMPSJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestIMPSJson): AccountPayinRequestIMPS {
        val builder = AccountPayinRequestIMPS.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestIMPS): AccountPayinRequestIMPSJson {
        val json = AccountPayinRequestIMPSJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestIMPS): AccountPayinRequestIMPSImpl {
        return model as AccountPayinRequestIMPSImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestIMPSImpl): AccountPayinRequestIMPS {
        return impl
    }

}