//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class PayoutJson {
    var paymentRequested: PaymentRequested? = null
    var paymentMethod: PayoutMethod? = null
    var callbackUrl: String? = null
    var customerIp: String? = null
}