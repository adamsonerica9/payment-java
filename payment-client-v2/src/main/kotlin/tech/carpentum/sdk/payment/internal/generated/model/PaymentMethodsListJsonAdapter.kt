//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentMethodsList

class PaymentMethodsListJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentMethodsListJson): PaymentMethodsList {
        val builder = PaymentMethodsList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentMethodsList): PaymentMethodsListJson {
        val json = PaymentMethodsListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentMethodsList): PaymentMethodsListImpl {
        return model as PaymentMethodsListImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentMethodsListImpl): PaymentMethodsList {
        return impl
    }

}