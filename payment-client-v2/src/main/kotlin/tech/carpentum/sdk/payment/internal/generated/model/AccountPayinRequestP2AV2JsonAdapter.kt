//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestP2AV2

class AccountPayinRequestP2AV2JsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestP2AV2Json): AccountPayinRequestP2AV2 {
        val builder = AccountPayinRequestP2AV2.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestP2AV2): AccountPayinRequestP2AV2Json {
        val json = AccountPayinRequestP2AV2Json()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestP2AV2): AccountPayinRequestP2AV2Impl {
        return model as AccountPayinRequestP2AV2Impl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestP2AV2Impl): AccountPayinRequestP2AV2 {
        return impl
    }

}