//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * Parameters of a customer's wallet information where your customer would like his funds to be transferred.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AccountPayoutRequestWalletTransferImpl implements AccountPayoutRequestWalletTransfer {
    /** Name of a person who holds a wallet where your customer would like his funds to be transferred.

If currency is INR, then accountName field is required. Otherwise, it is optional. */
    private final Optional<String> accountName;

    @Override
    public Optional<String> getAccountName() {
        return accountName;
    }


    /** Address of customer's wallet where your customer would like his funds to be transferred. */
    private final String accountNumber;

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }




    private final int hashCode;
    private final String toString;

    private AccountPayoutRequestWalletTransferImpl(BuilderImpl builder) {
        this.accountName = Optional.ofNullable(builder.accountName);
        this.accountNumber = Objects.requireNonNull(builder.accountNumber, "Property 'accountNumber' is required.");

        this.hashCode = Objects.hash(accountName, accountNumber);
        this.toString = builder.type + "(" +
                "accountName=" + accountName +
                ", accountNumber=" + accountNumber +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AccountPayoutRequestWalletTransferImpl)) {
            return false;
        }

        AccountPayoutRequestWalletTransferImpl that = (AccountPayoutRequestWalletTransferImpl) obj;
        if (!this.accountName.equals(that.accountName)) return false;
        if (!this.accountNumber.equals(that.accountNumber)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AccountPayoutRequestWalletTransfer} model class. */
    public static class BuilderImpl implements AccountPayoutRequestWalletTransfer.Builder {
        private String accountName = null;
        private String accountNumber = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AccountPayoutRequestWalletTransfer");
        }

        /**
          * Set {@link AccountPayoutRequestWalletTransfer#getAccountName} property.
          *
          * Name of a person who holds a wallet where your customer would like his funds to be transferred.

If currency is INR, then accountName field is required. Otherwise, it is optional.
          */
        @Override
        public BuilderImpl accountName(String accountName) {
            this.accountName = accountName;
            return this;
        }

        /**
          * Set {@link AccountPayoutRequestWalletTransfer#getAccountNumber} property.
          *
          * Address of customer's wallet where your customer would like his funds to be transferred.
          */
        @Override
        public BuilderImpl accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        /**
         * Create new instance of {@link AccountPayoutRequestWalletTransfer} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AccountPayoutRequestWalletTransferImpl build() {
            return new AccountPayoutRequestWalletTransferImpl(this);
        }

    }
}