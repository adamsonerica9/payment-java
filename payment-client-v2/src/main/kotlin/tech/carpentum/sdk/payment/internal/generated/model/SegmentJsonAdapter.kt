//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.Segment

class SegmentJsonAdapter {
    @FromJson
    fun fromJson(json: SegmentJson): Segment {
        val builder = Segment.builder()
        builder.code(json.code)
        return builder.build()
    }

    @ToJson
    fun toJson(model: Segment): SegmentJson {
        val json = SegmentJson()
        json.code = model.code
        return json
    }

    @FromJson
    fun fromJsonImpl(model: Segment): SegmentImpl {
        return model as SegmentImpl
    }

    @ToJson
    fun toJsonImpl(impl: SegmentImpl): Segment {
        return impl
    }

}