//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class ExternalReferenceImpl implements ExternalReference {
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }




    private final int hashCode;
    private final String toString;

    private ExternalReferenceImpl(BuilderImpl builder) {
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");

        this.hashCode = Objects.hash(reference);
        this.toString = builder.type + "(" +
                "reference=" + reference +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof ExternalReferenceImpl)) {
            return false;
        }

        ExternalReferenceImpl that = (ExternalReferenceImpl) obj;
        if (!this.reference.equals(that.reference)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link ExternalReference} model class. */
    public static class BuilderImpl implements ExternalReference.Builder {
        private String reference = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("ExternalReference");
        }

        /**
          * Set {@link ExternalReference#getReference} property.
          *
          * 
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        /**
         * Create new instance of {@link ExternalReference} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public ExternalReferenceImpl build() {
            return new ExternalReferenceImpl(this);
        }

    }
}