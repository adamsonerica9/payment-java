//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PromptPayMethod

class PromptPayMethodJsonAdapter {
    @FromJson
    fun fromJson(json: PromptPayMethodJson): PromptPayMethod {
        val builder = PromptPayMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PromptPayMethod): PromptPayMethodJson {
        val json = PromptPayMethodJson()
        json.account = model.account.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PromptPayMethod): PromptPayMethodImpl {
        return model as PromptPayMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: PromptPayMethodImpl): PromptPayMethod {
        return impl
    }

}