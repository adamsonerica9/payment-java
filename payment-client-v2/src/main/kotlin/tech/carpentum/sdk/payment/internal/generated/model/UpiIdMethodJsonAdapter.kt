//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.UpiIdMethod

class UpiIdMethodJsonAdapter {
    @FromJson
    fun fromJson(json: UpiIdMethodJson): UpiIdMethod {
        val builder = UpiIdMethod.builder()
        builder.upiId(json.upiId)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: UpiIdMethod): UpiIdMethodJson {
        val json = UpiIdMethodJson()
        json.upiId = model.upiId
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: UpiIdMethod): UpiIdMethodImpl {
        return model as UpiIdMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: UpiIdMethodImpl): UpiIdMethod {
        return impl
    }

}