//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentStatus

class PaymentStatusJsonAdapter {

    @FromJson
    fun fromJson(json: String): PaymentStatus {
        return PaymentStatus.of(json)
    }

    @ToJson
    fun toJson(model: PaymentStatus): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: PaymentStatus): PaymentStatusImpl {
        return model as PaymentStatusImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentStatusImpl): PaymentStatus {
        return impl
    }

}