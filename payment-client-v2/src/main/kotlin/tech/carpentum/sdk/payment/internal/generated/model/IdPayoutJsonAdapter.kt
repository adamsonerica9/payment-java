//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.IdPayout

class IdPayoutJsonAdapter {

    @FromJson
    fun fromJson(json: String): IdPayout {
        return IdPayout.of(json)
    }

    @ToJson
    fun toJson(model: IdPayout): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: IdPayout): IdPayoutImpl {
        return model as IdPayoutImpl
    }

    @ToJson
    fun toJsonImpl(impl: IdPayoutImpl): IdPayout {
        return impl
    }

}