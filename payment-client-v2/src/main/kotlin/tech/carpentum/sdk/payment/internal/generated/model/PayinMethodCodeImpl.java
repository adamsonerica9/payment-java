//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * Payment method is a payment instrument that your customers use (such as online bank transfers, VA payments, UPI and so on).
Select a payment method from the list of the methods that you have configured in your account.
The `PaymentMethodCode` represents payment method available for a payment.

 - OFFLINE - Offline Bank Transfer. Payment method which requires customer to copy Payment instruction from the Payment application right after the incoming payment is submitted and create the Payment transfer using the instructions within customer's own payment service such as Internet or mobile banking or wallet.

   As the Payment relies on the customer offline Payment transfer processing it can take minutes for Payment to be confirmed and also number of Payment expiration ratio can be higher.
 - ONLINE - Online Bank Transfer. Payment method where Customer is redirected to his own preferred Payment service application right after the payment is submitted and create the Payment transfer with pre-filled Payment instruction.
 - VAPAY - Virtual Accounts. Payment method which requires customer to copy Payment instruction with generated Virtual Account from the Payment application right after the payment is submitted and create the Payment transfer using the instructions within customer's own payment service such as Internet or mobile banking, wallet or ATM.

   One Virtual account cannot be used for sending funds repeatedly.
 - P2A_V2 - Phone to Account. Payment method which requires customer to scan QR code from the Payment application by Customer's Payment service application right after the payment is submitted.

   Supported Payment service applications are UPI and GooglePay both on India market.
 - IMPS - Is an India market version of the Offline Bank Transfer which differs in the data which need to be collected and used during a Payment process.
 - PROMPTPAY - A payment method which requires customer to scan QR code from the Payment application by Customer's Payment service application right after the payment is submitted.
 - QRISPAY - QRIS is a payment method intended for the Indonesian market which allows users to pay using scanning QR codes by theirs Payment application.
 - NETBANKING - Netbanking, also known as online banking or Net Banking is a payment option offered by banks and financial institutions on Indian market that allow customers to use banking services over the internet. In our case, we take advantage of the Netbanking services to offer your customer UPI Payment method to pay funds directly from your customer Net Banking account.
 - DUITNOW - A payment method which requires customer to scan QR code from the Payment application by Customer's Payment service application right after the payment is submitted.
 - CRYPTO_OFFLINE - Crypto Offline is a payment method that allows customers to pay by crypto currency from they crypto wallet to an address that they are provided.
 - UPIID - UPI (Unified Payment Method) is a method of fund transfers in India. The mechanism allows two parties to exchange funds using a UPI ID without having to share the banking information with the other party.
 *
 * 
 *
 * The model class is immutable.
 *
 * Use static {@link #of} method to create a new model class instance.
 */
@JsonClass(generateAdapter = false)
public class PayinMethodCodeImpl implements PayinMethodCode {
    private final @NotNull String value;

    private final int hashCode;
    private final String toString;

    public PayinMethodCodeImpl(@NotNull String value) {
        this.value = Objects.requireNonNull(value, "Property 'PayinMethodCode' is required.");

        this.hashCode = value.hashCode();
        this.toString = String.valueOf(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return toString;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PayinMethodCodeImpl)) {
            return false;
        }

        PayinMethodCodeImpl that = (PayinMethodCodeImpl) obj;
        if (!this.value.equals(that.value)) return false;

        return true;
    }
}