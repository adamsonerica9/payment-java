//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * @see VaPayMethodResponse
 * @see OfflineMethodResponse
 * @see CapitecPayMethodResponse
 * @see NetBankingMethodResponse
 * @see QrPhMethodResponse
 * @see VietQRMethodResponse
 * @see CryptoOfflineMethodResponse
 * @see P2AV2MethodResponse
 * @see OnlineMethodResponse
 * @see QrisPayMethodResponse
 * @see UpiIdMethodResponse
 * @see IMPSMethodResponse
 * @see DuitNowPayMethodResponse
 * @see EWalletMethodResponse
 * @see UpiQRMethodResponse
 * @see PromptPayMethodResponse
 *
 * The model class is immutable.
 *
 *
 */
public interface PayinMethodResponse {
    /** Name of discriminator property used to distinguish between "one-of" subclasses. */
    String DISCRIMINATOR = "paymentMethodCode";

    /**
     * @see PaymentMethodCode
     */
    @NotNull PaymentMethodCode getPaymentMethodCode();

    /**
     * Enumeration of "one-of" subclasses distinguished by {@code paymentMethodCode} discriminator value.
     */
    enum PaymentMethodCode {
        CAPITEC_PAY,
        CRYPTO_OFFLINE,
        DUITNOW,
        EWALLET,
        IMPS,
        NETBANKING,
        OFFLINE,
        ONLINE,
        P2A_V2,
        PROMPTPAY,
        QRISPAY,
        QRPH,
        UPIID,
        UPIQR,
        VAPAY,
        VIETQR,
    }
}