//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.Money

class MoneyJsonAdapter {
    @FromJson
    fun fromJson(json: MoneyJson): Money {
        val builder = Money.builder()
        builder.amount(json.amount)
        builder.currencyCode(json.currencyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: Money): MoneyJson {
        val json = MoneyJson()
        json.amount = model.amount
        json.currencyCode = model.currencyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: Money): MoneyImpl {
        return model as MoneyImpl
    }

    @ToJson
    fun toJsonImpl(impl: MoneyImpl): Money {
        return impl
    }

}