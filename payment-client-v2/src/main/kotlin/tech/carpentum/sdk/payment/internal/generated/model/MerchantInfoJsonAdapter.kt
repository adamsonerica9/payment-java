//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MerchantInfo

class MerchantInfoJsonAdapter {
    @FromJson
    fun fromJson(json: MerchantInfoJson): MerchantInfo {
        val builder = MerchantInfo.builder()
        builder.channelInfo(json.channelInfo)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MerchantInfo): MerchantInfoJson {
        val json = MerchantInfoJson()
        json.channelInfo = model.channelInfo
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MerchantInfo): MerchantInfoImpl {
        return model as MerchantInfoImpl
    }

    @ToJson
    fun toJsonImpl(impl: MerchantInfoImpl): MerchantInfo {
        return impl
    }

}