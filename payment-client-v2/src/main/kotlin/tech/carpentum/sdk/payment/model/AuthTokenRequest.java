//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AuthTokenRequest {

    /** Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration). */
    @NotNull String getMerchantCode();

    /** See [Merchant Secret Key](terminology.html#term-Merchant-secret). */
    @NotNull String getSecret();

    /** Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes). */
    @NotNull Optional<Integer> getValiditySecs();

    /** Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description. */
    @NotNull java.util.List<@NotNull String> getOperations();

    @NotNull Optional<MoneyAuthRequest> getMoney();

    @NotNull static Builder builder(AuthTokenRequest copyOf) {
        Builder builder = builder();
        builder.merchantCode(copyOf.getMerchantCode());
        builder.secret(copyOf.getSecret());
        builder.validitySecs(copyOf.getValiditySecs().orElse(null));
        builder.operations(copyOf.getOperations());
        builder.money(copyOf.getMoney().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new AuthTokenRequestImpl.BuilderImpl();
    }

    /** Builder for {@link AuthTokenRequest} model class. */
    interface Builder {

        /**
          * Set {@link AuthTokenRequest#getMerchantCode} property.
          *
          * Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration).
          */
        @NotNull Builder merchantCode(String merchantCode);


        /**
          * Set {@link AuthTokenRequest#getSecret} property.
          *
          * See [Merchant Secret Key](terminology.html#term-Merchant-secret).
          */
        @NotNull Builder secret(String secret);


        /**
          * Set {@link AuthTokenRequest#getValiditySecs} property.
          *
          * Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes).
          */
        @NotNull Builder validitySecs(Integer validitySecs);


        /**
          * Replace all items in {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @NotNull Builder operations(java.util.List<@NotNull String> operations);
        /**
          * Add single item to {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @NotNull Builder operationsAdd(String item);
        /**
          * Add all items to {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @NotNull Builder operationsAddAll(java.util.List<@NotNull String> operations);


        /**
          * Set {@link AuthTokenRequest#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(MoneyAuthRequest money);


        /**
         * Create new instance of {@link AuthTokenRequest} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AuthTokenRequest build();

    }
}