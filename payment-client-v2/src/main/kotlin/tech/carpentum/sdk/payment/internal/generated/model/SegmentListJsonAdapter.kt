//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.SegmentList

class SegmentListJsonAdapter {
    @FromJson
    fun fromJson(json: SegmentListJson): SegmentList {
        val builder = SegmentList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: SegmentList): SegmentListJson {
        val json = SegmentListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: SegmentList): SegmentListImpl {
        return model as SegmentListImpl
    }

    @ToJson
    fun toJsonImpl(impl: SegmentListImpl): SegmentList {
        return impl
    }

}