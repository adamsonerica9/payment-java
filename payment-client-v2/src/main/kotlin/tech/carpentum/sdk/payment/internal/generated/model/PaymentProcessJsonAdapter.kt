//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentProcess

class PaymentProcessJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentProcessJson): PaymentProcess {
        val builder = PaymentProcess.builder()
        builder.status(json.status)
        builder.failureReasons(json.failureReasons)
        builder.createdAt(json.createdAt)
        builder.processedAt(json.processedAt)
        builder.isTest(json.isTest)
        builder.processorStatus(json.processorStatus)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentProcess): PaymentProcessJson {
        val json = PaymentProcessJson()
        json.status = model.status
        json.failureReasons = model.failureReasons.orElse(null)
        json.createdAt = model.createdAt
        json.processedAt = model.processedAt.orElse(null)
        json.isTest = model.isTest
        json.processorStatus = model.processorStatus.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentProcess): PaymentProcessImpl {
        return model as PaymentProcessImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentProcessImpl): PaymentProcess {
        return impl
    }

}