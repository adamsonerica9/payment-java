//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.ChannelInfo

class ChannelInfoJsonAdapter {
    @FromJson
    fun fromJson(json: ChannelInfoJson): ChannelInfo {
        val builder = ChannelInfo.builder()
        builder.name(json.name)
        return builder.build()
    }

    @ToJson
    fun toJson(model: ChannelInfo): ChannelInfoJson {
        val json = ChannelInfoJson()
        json.name = model.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: ChannelInfo): ChannelInfoImpl {
        return model as ChannelInfoImpl
    }

    @ToJson
    fun toJsonImpl(impl: ChannelInfoImpl): ChannelInfo {
        return impl
    }

}