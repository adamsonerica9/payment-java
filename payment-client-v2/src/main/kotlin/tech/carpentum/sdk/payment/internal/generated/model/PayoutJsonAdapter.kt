//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.Payout

class PayoutJsonAdapter {
    @FromJson
    fun fromJson(json: PayoutJson): Payout {
        val builder = Payout.builder()
        builder.paymentRequested(json.paymentRequested)
        builder.paymentMethod(json.paymentMethod)
        builder.callbackUrl(json.callbackUrl)
        builder.customerIp(json.customerIp)
        return builder.build()
    }

    @ToJson
    fun toJson(model: Payout): PayoutJson {
        val json = PayoutJson()
        json.paymentRequested = model.paymentRequested
        json.paymentMethod = model.paymentMethod
        json.callbackUrl = model.callbackUrl.orElse(null)
        json.customerIp = model.customerIp.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: Payout): PayoutImpl {
        return model as PayoutImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayoutImpl): Payout {
        return impl
    }

}