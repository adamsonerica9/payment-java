//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CustomerTransactionFee

class CustomerTransactionFeeJsonAdapter {
    @FromJson
    fun fromJson(json: CustomerTransactionFeeJson): CustomerTransactionFee {
        val builder = CustomerTransactionFee.builder()
        builder.minFee(json.minFee)
        builder.maxFee(json.maxFee)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CustomerTransactionFee): CustomerTransactionFeeJson {
        val json = CustomerTransactionFeeJson()
        json.minFee = model.minFee.orElse(null)
        json.maxFee = model.maxFee.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CustomerTransactionFee): CustomerTransactionFeeImpl {
        return model as CustomerTransactionFeeImpl
    }

    @ToJson
    fun toJsonImpl(impl: CustomerTransactionFeeImpl): CustomerTransactionFee {
        return impl
    }

}