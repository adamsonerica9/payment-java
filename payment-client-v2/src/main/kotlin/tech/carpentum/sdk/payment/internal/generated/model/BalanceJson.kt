//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class BalanceJson {
    var currencyCode: CurrencyCode? = null
    var balance: java.math.BigDecimal? = null
    var availableBalance: java.math.BigDecimal? = null
    var pendingSettlementAmount: java.math.BigDecimal? = null
    var pendingPayoutAmount: java.math.BigDecimal? = null
    var lastBalanceMovement: java.time.OffsetDateTime? = null
}