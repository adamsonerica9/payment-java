//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.QrisPayMethod

class QrisPayMethodJsonAdapter {
    @FromJson
    fun fromJson(json: QrisPayMethodJson): QrisPayMethod {
        val builder = QrisPayMethod.builder()
        builder.account(json.account)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: QrisPayMethod): QrisPayMethodJson {
        val json = QrisPayMethodJson()
        json.account = model.account.orElse(null)
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: QrisPayMethod): QrisPayMethodImpl {
        return model as QrisPayMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: QrisPayMethodImpl): QrisPayMethod {
        return impl
    }

}