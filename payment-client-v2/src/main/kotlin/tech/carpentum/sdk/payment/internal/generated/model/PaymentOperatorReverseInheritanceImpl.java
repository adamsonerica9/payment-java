//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@Deprecated
@JsonClass(generateAdapter = false)
public class PaymentOperatorReverseInheritanceImpl implements PaymentOperator.ReverseInheritance {
    private final String code;

    @Override
    public String getCode() {
        return code;
    }


    private final String name;

    @Override
    public String getName() {
        return name;
    }


    private final Optional<CustomerTransactionFee> customerTransactionFee;

    @Override
    public Optional<CustomerTransactionFee> getCustomerTransactionFee() {
        return customerTransactionFee;
    }




    private final int hashCode;
    private final String toString;

    private PaymentOperatorReverseInheritanceImpl(BuilderImpl builder) {
        this.code = Objects.requireNonNull(builder.code, "Property 'code' is required.");
        this.name = Objects.requireNonNull(builder.name, "Property 'name' is required.");
        this.customerTransactionFee = Optional.ofNullable(builder.customerTransactionFee);

        this.hashCode = Objects.hash(code, name, customerTransactionFee);
        this.toString = builder.type + "(" +
                "code=" + code +
                ", name=" + name +
                ", customerTransactionFee=" + customerTransactionFee +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentOperatorReverseInheritanceImpl)) {
            return false;
        }

        PaymentOperatorReverseInheritanceImpl that = (PaymentOperatorReverseInheritanceImpl) obj;
        if (!this.code.equals(that.code)) return false;
        if (!this.name.equals(that.name)) return false;
        if (!this.customerTransactionFee.equals(that.customerTransactionFee)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentOperator} model class. */
    public static class BuilderImpl implements PaymentOperatorIncoming.Builder, PaymentOperatorOutgoing.Builder, PaymentOperator.Builder {
        private String code = null;
        private String name = null;
        private CustomerTransactionFee customerTransactionFee = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentOperator");
        }

        /**
          * Set {@link PaymentOperator#getCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl code(String code) {
            this.code = code;
            return this;
        }

        /**
          * Set {@link PaymentOperator#getName} property.
          *
          * 
          */
        @Override
        public BuilderImpl name(String name) {
            this.name = name;
            return this;
        }

        /**
          * Set {@link PaymentOperator#getCustomerTransactionFee} property.
          *
          * 
          */
        @Override
        public BuilderImpl customerTransactionFee(CustomerTransactionFee customerTransactionFee) {
            this.customerTransactionFee = customerTransactionFee;
            return this;
        }

        /**
         * Create new instance of {@link PaymentOperator} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentOperatorReverseInheritanceImpl build() {
            return new PaymentOperatorReverseInheritanceImpl(this);
        }

    }
}