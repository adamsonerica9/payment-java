//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.Currency

class CurrencyJsonAdapter {
    @FromJson
    fun fromJson(json: CurrencyJson): Currency {
        val builder = Currency.builder()
        builder.code(json.code)
        return builder.build()
    }

    @ToJson
    fun toJson(model: Currency): CurrencyJson {
        val json = CurrencyJson()
        json.code = model.code
        return json
    }

    @FromJson
    fun fromJsonImpl(model: Currency): CurrencyImpl {
        return model as CurrencyImpl
    }

    @ToJson
    fun toJsonImpl(impl: CurrencyImpl): Currency {
        return impl
    }

}