//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.FailureReasonCode

class FailureReasonCodeJsonAdapter {

    @FromJson
    fun fromJson(json: String): FailureReasonCode {
        return FailureReasonCode.of(json)
    }

    @ToJson
    fun toJson(model: FailureReasonCode): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: FailureReasonCode): FailureReasonCodeImpl {
        return model as FailureReasonCodeImpl
    }

    @ToJson
    fun toJsonImpl(impl: FailureReasonCodeImpl): FailureReasonCode {
        return impl
    }

}