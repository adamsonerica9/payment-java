//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.IntervalNumberTo

class IntervalNumberToJsonAdapter {
    @FromJson
    fun fromJson(json: IntervalNumberToJson): IntervalNumberTo {
        val builder = IntervalNumberTo.builder()
        builder.from(json.from)
        builder.to(json.to)
        return builder.build()
    }

    @ToJson
    fun toJson(model: IntervalNumberTo): IntervalNumberToJson {
        val json = IntervalNumberToJson()
        json.from = model.from.orElse(null)
        json.to = model.to.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: IntervalNumberTo): IntervalNumberToImpl {
        return model as IntervalNumberToImpl
    }

    @ToJson
    fun toJsonImpl(impl: IntervalNumberToImpl): IntervalNumberTo {
        return impl
    }

}