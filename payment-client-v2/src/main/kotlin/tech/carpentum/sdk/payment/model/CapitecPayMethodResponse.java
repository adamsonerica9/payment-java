//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CAPITEC_PAY
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface CapitecPayMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.CAPITEC_PAY;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull Money getMoney();

    @NotNull Optional<MoneyVat> getVat();

    @NotNull String getMerchantName();

    /** Your customer mobile phone number in full international telephone number format, including country code. */
    @NotNull String getPhoneNumber();

    /** Reference number of transaction. */
    @NotNull String getReference();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(CapitecPayMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.money(copyOf.getMoney());
        builder.vat(copyOf.getVat().orElse(null));
        builder.merchantName(copyOf.getMerchantName());
        builder.phoneNumber(copyOf.getPhoneNumber());
        builder.reference(copyOf.getReference());
        builder.returnUrl(copyOf.getReturnUrl());
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new CapitecPayMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link CapitecPayMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link CapitecPayMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);


        /**
          * Set {@link CapitecPayMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);


        /**
          * Set {@link CapitecPayMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(Money money);


        /**
          * Set {@link CapitecPayMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);


        /**
          * Set {@link CapitecPayMethodResponse#getMerchantName} property.
          *
          * 
          */
        @NotNull Builder merchantName(String merchantName);


        /**
          * Set {@link CapitecPayMethodResponse#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @NotNull Builder phoneNumber(String phoneNumber);


        /**
          * Set {@link CapitecPayMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);


        /**
          * Set {@link CapitecPayMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);


        /**
          * Set {@link CapitecPayMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);


        /**
          * Set {@link CapitecPayMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);


        /**
         * Create new instance of {@link CapitecPayMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull CapitecPayMethodResponse build();

    }
}