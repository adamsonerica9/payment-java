//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.IMPSMethod

class IMPSMethodJsonAdapter {
    @FromJson
    fun fromJson(json: IMPSMethodJson): IMPSMethod {
        val builder = IMPSMethod.builder()
        builder.account(json.account)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: IMPSMethod): IMPSMethodJson {
        val json = IMPSMethodJson()
        json.account = model.account
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: IMPSMethod): IMPSMethodImpl {
        return model as IMPSMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: IMPSMethodImpl): IMPSMethod {
        return impl
    }

}