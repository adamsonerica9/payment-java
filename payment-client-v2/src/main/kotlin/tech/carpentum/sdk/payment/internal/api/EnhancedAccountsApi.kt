package tech.carpentum.sdk.payment.internal.api

import tech.carpentum.sdk.payment.internal.generated.api.AccountsApi
import tech.carpentum.sdk.payment.internal.generated.infrastructure.RequestConfig
import java.time.Duration

internal class EnhancedAccountsApi(
    basePath: String = defaultBasePath,
    private val accessToken: String,
    callTimeout: Duration
) : AccountsApi(basePath, ApiUtils.getClient(callTimeout)) {

    override fun <T> updateAuthParams(requestConfig: RequestConfig<T>) {
        requestConfig.addAuthorizationHeader(accessToken)
    }
}
