//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestDuitNow

class AccountPayinRequestDuitNowJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestDuitNowJson): AccountPayinRequestDuitNow {
        val builder = AccountPayinRequestDuitNow.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestDuitNow): AccountPayinRequestDuitNowJson {
        val json = AccountPayinRequestDuitNowJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestDuitNow): AccountPayinRequestDuitNowImpl {
        return model as AccountPayinRequestDuitNowImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestDuitNowImpl): AccountPayinRequestDuitNow {
        return impl
    }

}