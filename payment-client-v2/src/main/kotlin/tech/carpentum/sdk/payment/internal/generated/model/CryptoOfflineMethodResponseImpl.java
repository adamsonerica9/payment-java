//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CRYPTO_OFFLINE
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class CryptoOfflineMethodResponseImpl implements CryptoOfflineMethodResponse {
    private final IdPayin idPayin;

    @Override
    public IdPayin getIdPayin() {
        return idPayin;
    }


    private final IdPayment idPayment;

    @Override
    public IdPayment getIdPayment() {
        return idPayment;
    }


    private final AccountResponseCryptoOffline account;

    @Override
    public AccountResponseCryptoOffline getAccount() {
        return account;
    }


    private final Money money;

    @Override
    public Money getMoney() {
        return money;
    }


    private final Optional<MoneyVat> vat;

    @Override
    public Optional<MoneyVat> getVat() {
        return vat;
    }


    private final String merchantName;

    @Override
    public String getMerchantName() {
        return merchantName;
    }


    /** Reference number of transaction. */
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }


    /** The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer. */
    private final String qrName;

    @Override
    public String getQrName() {
        return qrName;
    }


    /** The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer. */
    private final String qrCode;

    @Override
    public String getQrCode() {
        return qrCode;
    }


    private final PaymentOperatorIncoming paymentOperator;

    @Override
    public PaymentOperatorIncoming getPaymentOperator() {
        return paymentOperator;
    }


    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    private final String returnUrl;

    @Override
    public String getReturnUrl() {
        return returnUrl;
    }


    /** Date and time when payment was accepted. */
    private final java.time.OffsetDateTime acceptedAt;

    @Override
    public java.time.OffsetDateTime getAcceptedAt() {
        return acceptedAt;
    }


    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    private final java.time.OffsetDateTime expireAt;

    @Override
    public java.time.OffsetDateTime getExpireAt() {
        return expireAt;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private CryptoOfflineMethodResponseImpl(BuilderImpl builder) {
        this.idPayin = Objects.requireNonNull(builder.idPayin, "Property 'idPayin' is required.");
        this.idPayment = Objects.requireNonNull(builder.idPayment, "Property 'idPayment' is required.");
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.vat = Optional.ofNullable(builder.vat);
        this.merchantName = Objects.requireNonNull(builder.merchantName, "Property 'merchantName' is required.");
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");
        this.qrName = Objects.requireNonNull(builder.qrName, "Property 'qrName' is required.");
        this.qrCode = Objects.requireNonNull(builder.qrCode, "Property 'qrCode' is required.");
        this.paymentOperator = Objects.requireNonNull(builder.paymentOperator, "Property 'paymentOperator' is required.");
        this.returnUrl = Objects.requireNonNull(builder.returnUrl, "Property 'returnUrl' is required.");
        this.acceptedAt = Objects.requireNonNull(builder.acceptedAt, "Property 'acceptedAt' is required.");
        this.expireAt = Objects.requireNonNull(builder.expireAt, "Property 'expireAt' is required.");

        this.hashCode = Objects.hash(idPayin, idPayment, account, money, vat, merchantName, reference, qrName, qrCode, paymentOperator, returnUrl, acceptedAt, expireAt);
        this.toString = builder.type + "(" +
                "idPayin=" + idPayin +
                ", idPayment=" + idPayment +
                ", account=" + account +
                ", money=" + money +
                ", vat=" + vat +
                ", merchantName=" + merchantName +
                ", reference=" + reference +
                ", qrName=" + qrName +
                ", qrCode=" + qrCode +
                ", paymentOperator=" + paymentOperator +
                ", returnUrl=" + returnUrl +
                ", acceptedAt=" + acceptedAt +
                ", expireAt=" + expireAt +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CryptoOfflineMethodResponseImpl)) {
            return false;
        }

        CryptoOfflineMethodResponseImpl that = (CryptoOfflineMethodResponseImpl) obj;
        if (!this.idPayin.equals(that.idPayin)) return false;
        if (!this.idPayment.equals(that.idPayment)) return false;
        if (!this.account.equals(that.account)) return false;
        if (!this.money.equals(that.money)) return false;
        if (!this.vat.equals(that.vat)) return false;
        if (!this.merchantName.equals(that.merchantName)) return false;
        if (!this.reference.equals(that.reference)) return false;
        if (!this.qrName.equals(that.qrName)) return false;
        if (!this.qrCode.equals(that.qrCode)) return false;
        if (!this.paymentOperator.equals(that.paymentOperator)) return false;
        if (!this.returnUrl.equals(that.returnUrl)) return false;
        if (!this.acceptedAt.equals(that.acceptedAt)) return false;
        if (!this.expireAt.equals(that.expireAt)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link CryptoOfflineMethodResponse} model class. */
    public static class BuilderImpl implements CryptoOfflineMethodResponse.Builder {
        private IdPayin idPayin = null;
        private IdPayment idPayment = null;
        private AccountResponseCryptoOffline account = null;
        private Money money = null;
        private MoneyVat vat = null;
        private String merchantName = null;
        private String reference = null;
        private String qrName = null;
        private String qrCode = null;
        private PaymentOperatorIncoming paymentOperator = null;
        private String returnUrl = null;
        private java.time.OffsetDateTime acceptedAt = null;
        private java.time.OffsetDateTime expireAt = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("CryptoOfflineMethodResponse");
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getIdPayin} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayin(IdPayin idPayin) {
            this.idPayin = idPayin;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getIdPayment} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayment(IdPayment idPayment) {
            this.idPayment = idPayment;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountResponseCryptoOffline account) {
            this.account = account;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(Money money) {
            this.money = money;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getVat} property.
          *
          * 
          */
        @Override
        public BuilderImpl vat(MoneyVat vat) {
            this.vat = vat;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getMerchantName} property.
          *
          * 
          */
        @Override
        public BuilderImpl merchantName(String merchantName) {
            this.merchantName = merchantName;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getQrName} property.
          *
          * The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @Override
        public BuilderImpl qrName(String qrName) {
            this.qrName = qrName;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getQrCode} property.
          *
          * The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @Override
        public BuilderImpl qrCode(String qrCode) {
            this.qrCode = qrCode;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentOperator(PaymentOperatorIncoming paymentOperator) {
            this.paymentOperator = paymentOperator;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl returnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @Override
        public BuilderImpl acceptedAt(java.time.OffsetDateTime acceptedAt) {
            this.acceptedAt = acceptedAt;
            return this;
        }

        /**
          * Set {@link CryptoOfflineMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @Override
        public BuilderImpl expireAt(java.time.OffsetDateTime expireAt) {
            this.expireAt = expireAt;
            return this;
        }

        /**
         * Create new instance of {@link CryptoOfflineMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public CryptoOfflineMethodResponseImpl build() {
            return new CryptoOfflineMethodResponseImpl(this);
        }

    }
}