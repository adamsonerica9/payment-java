//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MoneyRequired

class MoneyRequiredJsonAdapter {
    @FromJson
    fun fromJson(json: MoneyRequiredJson): MoneyRequired {
        val builder = MoneyRequired.builder()
        builder.amount(json.amount)
        builder.currencyCode(json.currencyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MoneyRequired): MoneyRequiredJson {
        val json = MoneyRequiredJson()
        json.amount = model.amount
        json.currencyCode = model.currencyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MoneyRequired): MoneyRequiredImpl {
        return model as MoneyRequiredImpl
    }

    @ToJson
    fun toJsonImpl(impl: MoneyRequiredImpl): MoneyRequired {
        return impl
    }

}