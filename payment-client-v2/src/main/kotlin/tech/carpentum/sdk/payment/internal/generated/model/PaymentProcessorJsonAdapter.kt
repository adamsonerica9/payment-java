//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentProcessor

class PaymentProcessorJsonAdapter {

    @FromJson
    fun fromJson(json: String): PaymentProcessor {
        return PaymentProcessor.of(json)
    }

    @ToJson
    fun toJson(model: PaymentProcessor): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: PaymentProcessor): PaymentProcessorImpl {
        return model as PaymentProcessorImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentProcessorImpl): PaymentProcessor {
        return impl
    }

}