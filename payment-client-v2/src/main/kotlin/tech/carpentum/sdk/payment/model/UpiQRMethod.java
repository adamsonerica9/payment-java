//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** UPIQR
 *
 * UPI QR is payment method intended for the Indian market. This payment method requires customers to scan QR code by Customer's Payment service application compatible with UPI payment schema.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface UpiQRMethod extends PayinMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethod}. */
    PayinMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethod.PaymentMethodCode.UPIQR;

    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    @NotNull Optional<String> getEmailAddress();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull Optional<String> getPaymentOperatorCode();

    @NotNull static Builder builder(UpiQRMethod copyOf) {
        Builder builder = builder();
        builder.emailAddress(copyOf.getEmailAddress().orElse(null));
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new UpiQRMethodImpl.BuilderImpl();
    }

    /** Builder for {@link UpiQRMethod} model class. */
    interface Builder {

        /**
          * Set {@link UpiQRMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @NotNull Builder emailAddress(String emailAddress);


        /**
          * Set {@link UpiQRMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);


        /**
         * Create new instance of {@link UpiQRMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull UpiQRMethod build();

    }
}