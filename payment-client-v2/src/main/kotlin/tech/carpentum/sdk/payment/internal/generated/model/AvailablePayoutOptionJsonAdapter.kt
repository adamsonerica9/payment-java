//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailablePayoutOption

class AvailablePayoutOptionJsonAdapter {
    @FromJson
    fun fromJson(json: AvailablePayoutOptionJson): AvailablePayoutOption {
        val builder = AvailablePayoutOption.builder()
        builder.paymentMethodCode(json.paymentMethodCode)
        builder.paymentOperators(json.paymentOperators?.toList())
        builder.segmentCode(json.segmentCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailablePayoutOption): AvailablePayoutOptionJson {
        val json = AvailablePayoutOptionJson()
        json.paymentMethodCode = model.paymentMethodCode
        json.paymentOperators = model.paymentOperators.ifEmpty { null }
        json.segmentCode = model.segmentCode.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailablePayoutOption): AvailablePayoutOptionImpl {
        return model as AvailablePayoutOptionImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailablePayoutOptionImpl): AvailablePayoutOption {
        return impl
    }

}