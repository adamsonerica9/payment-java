//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** WALLET_TRANSFER
 *
 * Payment method for sending funds from your account to external wallet.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface WalletTransferMethod extends PayoutMethod {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayoutMethod}. */
    PayoutMethod.PaymentMethodCode PAYMENT_METHOD_CODE = PayoutMethod.PaymentMethodCode.WALLET_TRANSFER;

    @NotNull AccountPayoutRequestWalletTransfer getAccount();

    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    @NotNull Optional<String> getPaymentOperatorCode();

    @NotNull Optional<String> getRemark();

    @NotNull static Builder builder(WalletTransferMethod copyOf) {
        Builder builder = builder();
        builder.account(copyOf.getAccount());
        builder.paymentOperatorCode(copyOf.getPaymentOperatorCode().orElse(null));
        builder.remark(copyOf.getRemark().orElse(null));
        return builder;
    }

    @NotNull static Builder builder() {
        return new WalletTransferMethodImpl.BuilderImpl();
    }

    /** Builder for {@link WalletTransferMethod} model class. */
    interface Builder {

        /**
          * Set {@link WalletTransferMethod#getAccount} property.
          *
          * 
          */
        @NotNull Builder account(AccountPayoutRequestWalletTransfer account);


        /**
          * Set {@link WalletTransferMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @NotNull Builder paymentOperatorCode(String paymentOperatorCode);


        /**
          * Set {@link WalletTransferMethod#getRemark} property.
          *
          * 
          */
        @NotNull Builder remark(String remark);


        /**
         * Create new instance of {@link WalletTransferMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull WalletTransferMethod build();

    }
}