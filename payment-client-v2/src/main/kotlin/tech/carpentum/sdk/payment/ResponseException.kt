package tech.carpentum.sdk.payment

import tech.carpentum.sdk.payment.model.BasicError
import tech.carpentum.sdk.payment.model.BusinessValidationError

/**
 * Exception represents RESTful API call response error.
 *
 * See [ClientBasicErrorException], [ClientBusinessValidationErrorException], [ServerBasicErrorException].
 */
sealed class ResponseException(
    cause: Throwable,
    val statusCode: Int,
    message: String? = null
) : RuntimeException(message, cause)

/**
 * Exception represents RESTful API call response `4xx` (client) error.
 *
 * See [ClientBasicErrorException], [ClientBusinessValidationErrorException].
 */
open class ClientErrorException(cause: Throwable, statusCode: Int, message: String? = null) : ResponseException(cause, statusCode, message)

/**
 * Exception represents RESTful API call response `4xx` (client) error with [BasicError] response body.
 */
class ClientBasicErrorException(
    cause: Throwable,
    statusCode: Int,
    val basicError: BasicError
) : ClientErrorException(cause, statusCode) {
    override fun toString(): String {
        return "ClientBasicErrorException(statusCode='$statusCode', basicError=$basicError)"
    }
}

/**
 * Exception represents RESTful API call response `406` (client) error with [BusinessValidationError] response body subclass.
 */
abstract class ClientBusinessValidationErrorException(
    cause: Throwable,
    statusCode: Int,
    open val businessValidationErrors: Map<String, BusinessValidationError>
) : ClientErrorException(cause, statusCode) {
    override fun toString(): String {
        return "${this.javaClass.simpleName}(statusCode='$statusCode', businessValidationErrors=$businessValidationErrors)"
    }
}

/**
 * Exception represents RESTful API call response `5xx` (server) error.
 *
 * See [ServerBasicErrorException].
 */
open class ServerErrorException(cause: Throwable, statusCode: Int, message: String? = null) : ResponseException(cause, statusCode, message)

/**
 * Exception represents RESTful API call response `5xx` (server) error with [BasicError] response body.
 */
class ServerBasicErrorException(
    cause: Throwable,
    statusCode: Int,
    val basicError: BasicError
) : ServerErrorException(cause, statusCode) {
    override fun toString(): String {
        return "ServerBasicErrorException(statusCode='$statusCode', basicError=$basicError)"
    }
}
