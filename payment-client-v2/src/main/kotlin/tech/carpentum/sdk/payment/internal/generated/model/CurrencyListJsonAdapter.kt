//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CurrencyList

class CurrencyListJsonAdapter {
    @FromJson
    fun fromJson(json: CurrencyListJson): CurrencyList {
        val builder = CurrencyList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: CurrencyList): CurrencyListJson {
        val json = CurrencyListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CurrencyList): CurrencyListImpl {
        return model as CurrencyListImpl
    }

    @ToJson
    fun toJsonImpl(impl: CurrencyListImpl): CurrencyList {
        return impl
    }

}