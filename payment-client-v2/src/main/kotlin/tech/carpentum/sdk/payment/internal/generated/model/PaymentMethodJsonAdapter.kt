//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentMethod

class PaymentMethodJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentMethodJson): PaymentMethod {
        val builder = PaymentMethod.builder()
        builder.code(json.code)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentMethod): PaymentMethodJson {
        val json = PaymentMethodJson()
        json.code = model.code
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentMethod): PaymentMethodImpl {
        return model as PaymentMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentMethodImpl): PaymentMethod {
        return impl
    }

}