//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@Deprecated
@JsonClass(generateAdapter = false)
public interface PaymentOperator {

    @NotNull String getCode();

    @NotNull String getName();

    @NotNull Optional<CustomerTransactionFee> getCustomerTransactionFee();


    @Deprecated @NotNull static Builder builder() {
        return new PaymentOperatorReverseInheritanceImpl.BuilderImpl("PaymentOperator");
    }

    /** Builder for {@link PaymentOperator} model class. */
    @Deprecated interface Builder {

        /**
          * Set {@link PaymentOperator#getCode} property.
          *
          * 
          */
        @Deprecated @NotNull Builder code(String code);


        /**
          * Set {@link PaymentOperator#getName} property.
          *
          * 
          */
        @Deprecated @NotNull Builder name(String name);


        /**
          * Set {@link PaymentOperator#getCustomerTransactionFee} property.
          *
          * 
          */
        @Deprecated @NotNull Builder customerTransactionFee(CustomerTransactionFee customerTransactionFee);


        /**
         * Create new instance of {@link PaymentOperator} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Deprecated @NotNull PaymentOperator.ReverseInheritance build();

    }

    //
    // reverse inheritance
    //

    @Deprecated interface ReverseInheritance extends PaymentOperatorIncoming, PaymentOperatorOutgoing, PaymentOperator {}

}