//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CryptoTransferMethod

class CryptoTransferMethodJsonAdapter {
    @FromJson
    fun fromJson(json: CryptoTransferMethodJson): CryptoTransferMethod {
        val builder = CryptoTransferMethod.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.remark(json.remark)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CryptoTransferMethod): CryptoTransferMethodJson {
        val json = CryptoTransferMethodJson()
        json.account = model.account
        json.paymentOperatorCode = model.paymentOperatorCode
        json.remark = model.remark.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CryptoTransferMethod): CryptoTransferMethodImpl {
        return model as CryptoTransferMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: CryptoTransferMethodImpl): CryptoTransferMethod {
        return impl
    }

}