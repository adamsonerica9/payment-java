//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class FailureReasonsJson {
    var failedAt: java.time.OffsetDateTime? = null
    var failureReasonCode: FailureReasonCode? = null
    var message: String? = null
}