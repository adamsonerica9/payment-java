//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentRequested

class PaymentRequestedJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentRequestedJson): PaymentRequested {
        val builder = PaymentRequested.builder()
        builder.money(json.money)
        builder.segmentCode(json.segmentCode)
        builder.exchangedToCurrency(json.exchangedToCurrency)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentRequested): PaymentRequestedJson {
        val json = PaymentRequestedJson()
        json.money = model.money
        json.segmentCode = model.segmentCode.orElse(null)
        json.exchangedToCurrency = model.exchangedToCurrency.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentRequested): PaymentRequestedImpl {
        return model as PaymentRequestedImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentRequestedImpl): PaymentRequested {
        return impl
    }

}