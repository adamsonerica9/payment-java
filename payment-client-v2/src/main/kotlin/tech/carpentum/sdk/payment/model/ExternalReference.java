//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface ExternalReference {

    @NotNull String getReference();

    @NotNull static ExternalReference ofReference(String reference) { return builder().reference(reference).build(); }

    @NotNull static Builder builder() {
        return new ExternalReferenceImpl.BuilderImpl();
    }

    /** Builder for {@link ExternalReference} model class. */
    interface Builder {

        /**
          * Set {@link ExternalReference#getReference} property.
          *
          * 
          */
        @NotNull Builder reference(String reference);


        /**
         * Create new instance of {@link ExternalReference} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull ExternalReference build();

    }
}