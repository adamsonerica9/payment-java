//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** EWALLET
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface EWalletMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.EWALLET;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull Money getMoney();

    @NotNull Optional<MoneyVat> getVat();

    @NotNull String getMerchantName();

    /** Reference number of transaction. */
    @NotNull String getReference();

    @NotNull Optional<PaymentOperatorIncoming> getPaymentOperator();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(EWalletMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.money(copyOf.getMoney());
        builder.vat(copyOf.getVat().orElse(null));
        builder.merchantName(copyOf.getMerchantName());
        builder.reference(copyOf.getReference());
        builder.paymentOperator(copyOf.getPaymentOperator().orElse(null));
        builder.returnUrl(copyOf.getReturnUrl());
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new EWalletMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link EWalletMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link EWalletMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);


        /**
          * Set {@link EWalletMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);


        /**
          * Set {@link EWalletMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(Money money);


        /**
          * Set {@link EWalletMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);


        /**
          * Set {@link EWalletMethodResponse#getMerchantName} property.
          *
          * 
          */
        @NotNull Builder merchantName(String merchantName);


        /**
          * Set {@link EWalletMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);


        /**
          * Set {@link EWalletMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @NotNull Builder paymentOperator(PaymentOperatorIncoming paymentOperator);


        /**
          * Set {@link EWalletMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);


        /**
          * Set {@link EWalletMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);


        /**
          * Set {@link EWalletMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);


        /**
         * Create new instance of {@link EWalletMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull EWalletMethodResponse build();

    }
}