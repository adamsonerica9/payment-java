//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.VietQRMethodResponse

class VietQRMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: VietQRMethodResponseJson): VietQRMethodResponse {
        val builder = VietQRMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.reference(json.reference)
        builder.qrName(json.qrName)
        builder.qrCode(json.qrCode)
        builder.paymentOperator(json.paymentOperator)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: VietQRMethodResponse): VietQRMethodResponseJson {
        val json = VietQRMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.reference = model.reference
        json.qrName = model.qrName
        json.qrCode = model.qrCode
        json.paymentOperator = model.paymentOperator.orElse(null)
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: VietQRMethodResponse): VietQRMethodResponseImpl {
        return model as VietQRMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: VietQRMethodResponseImpl): VietQRMethodResponse {
        return impl
    }

}