//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CommaSeparatedCurrencyCodes

class CommaSeparatedCurrencyCodesJsonAdapter {

    @FromJson
    fun fromJson(json: String): CommaSeparatedCurrencyCodes {
        return CommaSeparatedCurrencyCodes.of(json)
    }

    @ToJson
    fun toJson(model: CommaSeparatedCurrencyCodes): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: CommaSeparatedCurrencyCodes): CommaSeparatedCurrencyCodesImpl {
        return model as CommaSeparatedCurrencyCodesImpl
    }

    @ToJson
    fun toJsonImpl(impl: CommaSeparatedCurrencyCodesImpl): CommaSeparatedCurrencyCodes {
        return impl
    }

}