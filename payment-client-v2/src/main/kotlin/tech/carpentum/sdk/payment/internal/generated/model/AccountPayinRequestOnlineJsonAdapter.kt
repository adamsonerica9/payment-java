//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestOnline

class AccountPayinRequestOnlineJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestOnlineJson): AccountPayinRequestOnline {
        val builder = AccountPayinRequestOnline.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestOnline): AccountPayinRequestOnlineJson {
        val json = AccountPayinRequestOnlineJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestOnline): AccountPayinRequestOnlineImpl {
        return model as AccountPayinRequestOnlineImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestOnlineImpl): AccountPayinRequestOnline {
        return impl
    }

}