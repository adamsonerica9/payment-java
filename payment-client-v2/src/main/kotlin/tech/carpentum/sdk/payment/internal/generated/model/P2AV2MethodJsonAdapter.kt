//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.P2AV2Method

class P2AV2MethodJsonAdapter {
    @FromJson
    fun fromJson(json: P2AV2MethodJson): P2AV2Method {
        val builder = P2AV2Method.builder()
        builder.account(json.account)
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: P2AV2Method): P2AV2MethodJson {
        val json = P2AV2MethodJson()
        json.account = model.account.orElse(null)
        json.paymentOperatorCode = model.paymentOperatorCode
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: P2AV2Method): P2AV2MethodImpl {
        return model as P2AV2MethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: P2AV2MethodImpl): P2AV2Method {
        return impl
    }

}