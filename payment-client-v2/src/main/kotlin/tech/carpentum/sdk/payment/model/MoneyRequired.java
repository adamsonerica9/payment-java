//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * Amount of funds which needs to paid by your customer to successfully process the payment. If it is not empty then We expect to receive this amount from your customer to process the payment order instead of the payment request amount however full payment request amount is added to your account balance.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface MoneyRequired {

    /** Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34).
The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted). */
    @NotNull java.math.BigDecimal getAmount();

    @NotNull CurrencyCode getCurrencyCode();

    @NotNull static Builder builder(MoneyRequired copyOf) {
        Builder builder = builder();
        builder.amount(copyOf.getAmount());
        builder.currencyCode(copyOf.getCurrencyCode());
        return builder;
    }

    @NotNull static Builder builder() {
        return new MoneyRequiredImpl.BuilderImpl();
    }

    /** Builder for {@link MoneyRequired} model class. */
    interface Builder {

        /**
          * Set {@link MoneyRequired#getAmount} property.
          *
          * Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34).
The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted).
          */
        @NotNull Builder amount(java.math.BigDecimal amount);


        /**
          * Set {@link MoneyRequired#getCurrencyCode} property.
          *
          * 
          */
        @NotNull Builder currencyCode(CurrencyCode currencyCode);


        /**
         * Create new instance of {@link MoneyRequired} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull MoneyRequired build();

    }
}