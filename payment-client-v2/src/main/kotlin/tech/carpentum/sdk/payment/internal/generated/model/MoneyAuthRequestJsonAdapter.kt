//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MoneyAuthRequest

class MoneyAuthRequestJsonAdapter {
    @FromJson
    fun fromJson(json: MoneyAuthRequestJson): MoneyAuthRequest {
        val builder = MoneyAuthRequest.builder()
        builder.amount(json.amount)
        builder.currencyCode(json.currencyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MoneyAuthRequest): MoneyAuthRequestJson {
        val json = MoneyAuthRequestJson()
        json.amount = model.amount
        json.currencyCode = model.currencyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MoneyAuthRequest): MoneyAuthRequestImpl {
        return model as MoneyAuthRequestImpl
    }

    @ToJson
    fun toJsonImpl(impl: MoneyAuthRequestImpl): MoneyAuthRequest {
        return impl
    }

}