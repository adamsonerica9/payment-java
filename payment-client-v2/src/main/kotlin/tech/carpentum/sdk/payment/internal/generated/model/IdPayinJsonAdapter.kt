//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.IdPayin

class IdPayinJsonAdapter {

    @FromJson
    fun fromJson(json: String): IdPayin {
        return IdPayin.of(json)
    }

    @ToJson
    fun toJson(model: IdPayin): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: IdPayin): IdPayinImpl {
        return model as IdPayinImpl
    }

    @ToJson
    fun toJsonImpl(impl: IdPayinImpl): IdPayin {
        return impl
    }

}