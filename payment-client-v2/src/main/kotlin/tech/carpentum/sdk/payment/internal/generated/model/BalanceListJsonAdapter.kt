//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.BalanceList

class BalanceListJsonAdapter {
    @FromJson
    fun fromJson(json: BalanceListJson): BalanceList {
        val builder = BalanceList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: BalanceList): BalanceListJson {
        val json = BalanceListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: BalanceList): BalanceListImpl {
        return model as BalanceListImpl
    }

    @ToJson
    fun toJsonImpl(impl: BalanceListImpl): BalanceList {
        return impl
    }

}