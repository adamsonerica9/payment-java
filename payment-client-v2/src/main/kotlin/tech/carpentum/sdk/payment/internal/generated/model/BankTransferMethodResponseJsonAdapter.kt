//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.BankTransferMethodResponse

class BankTransferMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: BankTransferMethodResponseJson): BankTransferMethodResponse {
        val builder = BankTransferMethodResponse.builder()
        builder.idPayout(json.idPayout)
        builder.idPayment(json.idPayment)
        builder.reference(json.reference)
        return builder.build()
    }

    @ToJson
    fun toJson(model: BankTransferMethodResponse): BankTransferMethodResponseJson {
        val json = BankTransferMethodResponseJson()
        json.idPayout = model.idPayout
        json.idPayment = model.idPayment
        json.paymentMethodCode = model.paymentMethodCode.name
        json.reference = model.reference
        return json
    }

    @FromJson
    fun fromJsonImpl(model: BankTransferMethodResponse): BankTransferMethodResponseImpl {
        return model as BankTransferMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: BankTransferMethodResponseImpl): BankTransferMethodResponse {
        return impl
    }

}