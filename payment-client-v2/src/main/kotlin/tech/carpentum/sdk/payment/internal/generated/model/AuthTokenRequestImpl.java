//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class AuthTokenRequestImpl implements AuthTokenRequest {
    /** Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration). */
    private final String merchantCode;

    @Override
    public String getMerchantCode() {
        return merchantCode;
    }


    /** See [Merchant Secret Key](terminology.html#term-Merchant-secret). */
    private final String secret;

    @Override
    public String getSecret() {
        return secret;
    }


    /** Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes). */
    private final Optional<Integer> validitySecs;

    @Override
    public Optional<Integer> getValiditySecs() {
        return validitySecs;
    }


    /** Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description. */
    private final java.util.List<@NotNull String> operations;

    @Override
    public java.util.List<@NotNull String> getOperations() {
        return operations;
    }


    private final Optional<MoneyAuthRequest> money;

    @Override
    public Optional<MoneyAuthRequest> getMoney() {
        return money;
    }




    private final int hashCode;
    private final String toString;

    private AuthTokenRequestImpl(BuilderImpl builder) {
        this.merchantCode = Objects.requireNonNull(builder.merchantCode, "Property 'merchantCode' is required.");
        this.secret = Objects.requireNonNull(builder.secret, "Property 'secret' is required.");
        this.validitySecs = Optional.ofNullable(builder.validitySecs);
        this.operations = java.util.Collections.unmodifiableList(builder.operations);
        this.money = Optional.ofNullable(builder.money);

        this.hashCode = Objects.hash(merchantCode, secret, validitySecs, operations, money);
        this.toString = builder.type + "(" +
                "merchantCode=" + merchantCode +
                ", secret=" + secret +
                ", validitySecs=" + validitySecs +
                ", operations=" + operations +
                ", money=" + money +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AuthTokenRequestImpl)) {
            return false;
        }

        AuthTokenRequestImpl that = (AuthTokenRequestImpl) obj;
        if (!this.merchantCode.equals(that.merchantCode)) return false;
        if (!this.secret.equals(that.secret)) return false;
        if (!this.validitySecs.equals(that.validitySecs)) return false;
        if (!this.operations.equals(that.operations)) return false;
        if (!this.money.equals(that.money)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link AuthTokenRequest} model class. */
    public static class BuilderImpl implements AuthTokenRequest.Builder {
        private String merchantCode = null;
        private String secret = null;
        private Integer validitySecs = null;
        private java.util.List<@NotNull String> operations = new java.util.ArrayList<>();
        private MoneyAuthRequest money = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("AuthTokenRequest");
        }

        /**
          * Set {@link AuthTokenRequest#getMerchantCode} property.
          *
          * Your Merchant ID. This Merchant ID is obtained upon [Merchant enrollment process](sandbox.html#sandbox-account-registration).
          */
        @Override
        public BuilderImpl merchantCode(String merchantCode) {
            this.merchantCode = merchantCode;
            return this;
        }

        /**
          * Set {@link AuthTokenRequest#getSecret} property.
          *
          * See [Merchant Secret Key](terminology.html#term-Merchant-secret).
          */
        @Override
        public BuilderImpl secret(String secret) {
            this.secret = secret;
            return this;
        }

        /**
          * Set {@link AuthTokenRequest#getValiditySecs} property.
          *
          * Validity of the token in seconds. Maximum value 3600, default value 1800 (30 minutes).
          */
        @Override
        public BuilderImpl validitySecs(Integer validitySecs) {
            this.validitySecs = validitySecs;
            return this;
        }

        /**
          * Replace all items in {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @Override
        public BuilderImpl operations(java.util.List<@NotNull String> operations) {
            this.operations.clear();
            if (operations != null) {
                this.operations.addAll(operations);
            }
            return this;
        }
        /**
          * Add single item to {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @Override
        public BuilderImpl operationsAdd(String item) {
            if (item != null) {
                this.operations.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link AuthTokenRequest#getOperations} list property.
          *
          * Endpoints that can be called with the issued token. See [Fine-grained privileges](general.html#fine-grained-privileges) description.
          */
        @Override
        public BuilderImpl operationsAddAll(java.util.List<@NotNull String> operations) {
            if (operations != null) {
                this.operations.addAll(operations);
            }
            return this;
        }


        /**
          * Set {@link AuthTokenRequest#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(MoneyAuthRequest money) {
            this.money = money;
            return this;
        }

        /**
         * Create new instance of {@link AuthTokenRequest} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public AuthTokenRequestImpl build() {
            return new AuthTokenRequestImpl(this);
        }

    }
}