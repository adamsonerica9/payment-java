//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** CAPITEC_PAY
 *
 * Capitec Pay is an easy, fast and safe way to pay without having to enter the bank card details or share banking login information. The customer can choose the account he wants to pay from and authenticate the payment safely in the banking app.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class CapitecPayMethodImpl implements CapitecPayMethod {
    /** Your customer mobile phone number in full international telephone number format, including country code. */
    private final String phoneNumber;

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private CapitecPayMethodImpl(BuilderImpl builder) {
        this.phoneNumber = Objects.requireNonNull(builder.phoneNumber, "Property 'phoneNumber' is required.");

        this.hashCode = Objects.hash(phoneNumber);
        this.toString = builder.type + "(" +
                "phoneNumber=" + phoneNumber +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CapitecPayMethodImpl)) {
            return false;
        }

        CapitecPayMethodImpl that = (CapitecPayMethodImpl) obj;
        if (!this.phoneNumber.equals(that.phoneNumber)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link CapitecPayMethod} model class. */
    public static class BuilderImpl implements CapitecPayMethod.Builder {
        private String phoneNumber = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("CapitecPayMethod");
        }

        /**
          * Set {@link CapitecPayMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        /**
         * Create new instance of {@link CapitecPayMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public CapitecPayMethodImpl build() {
            return new CapitecPayMethodImpl(this);
        }

    }
}