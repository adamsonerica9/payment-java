//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentAddress

class PaymentAddressJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentAddressJson): PaymentAddress {
        val builder = PaymentAddress.builder()
        builder.name(json.name)
        builder.idAddress(json.idAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentAddress): PaymentAddressJson {
        val json = PaymentAddressJson()
        json.name = model.name
        json.idAddress = model.idAddress
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentAddress): PaymentAddressImpl {
        return model as PaymentAddressImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentAddressImpl): PaymentAddress {
        return impl
    }

}