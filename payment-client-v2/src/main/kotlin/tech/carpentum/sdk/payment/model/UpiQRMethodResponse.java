//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** UPIQR
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface UpiQRMethodResponse extends PayinMethodResponse {
    /** A discriminator value of property {@link #getPaymentMethodCode}. The model class extends {@link PayinMethodResponse}. */
    PayinMethodResponse.PaymentMethodCode PAYMENT_METHOD_CODE = PayinMethodResponse.PaymentMethodCode.UPIQR;

    @NotNull IdPayin getIdPayin();

    @NotNull IdPayment getIdPayment();

    @NotNull Money getMoney();

    @NotNull Optional<MoneyVat> getVat();

    @NotNull String getMerchantName();

    /** Reference number of transaction. */
    @NotNull String getReference();

    /** The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer. */
    @NotNull String getQrName();

    /** The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer. */
    @NotNull String getQrCode();

    /** It can be used as deep link button target (what is typically known as an intent trigger)
or to generate a QR code that can be scanned with any UPI enabled app. */
    @NotNull Optional<String> getUpiQrDeepLink();

    @NotNull Optional<PaymentOperatorIncoming> getPaymentOperator();

    /** Virtual payment address where we expect that your customer sends funds to make a payment. This parameter is to be shown to your customer. */
    @NotNull Optional<String> getUpiId();

    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    @NotNull String getReturnUrl();

    /** Date and time when payment was accepted. */
    @NotNull java.time.OffsetDateTime getAcceptedAt();

    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    @NotNull java.time.OffsetDateTime getExpireAt();

    @NotNull static Builder builder(UpiQRMethodResponse copyOf) {
        Builder builder = builder();
        builder.idPayin(copyOf.getIdPayin());
        builder.idPayment(copyOf.getIdPayment());
        builder.money(copyOf.getMoney());
        builder.vat(copyOf.getVat().orElse(null));
        builder.merchantName(copyOf.getMerchantName());
        builder.reference(copyOf.getReference());
        builder.qrName(copyOf.getQrName());
        builder.qrCode(copyOf.getQrCode());
        builder.upiQrDeepLink(copyOf.getUpiQrDeepLink().orElse(null));
        builder.paymentOperator(copyOf.getPaymentOperator().orElse(null));
        builder.upiId(copyOf.getUpiId().orElse(null));
        builder.returnUrl(copyOf.getReturnUrl());
        builder.acceptedAt(copyOf.getAcceptedAt());
        builder.expireAt(copyOf.getExpireAt());
        return builder;
    }

    @NotNull static Builder builder() {
        return new UpiQRMethodResponseImpl.BuilderImpl();
    }

    /** Builder for {@link UpiQRMethodResponse} model class. */
    interface Builder {

        /**
          * Set {@link UpiQRMethodResponse#getIdPayin} property.
          *
          * 
          */
        @NotNull Builder idPayin(IdPayin idPayin);


        /**
          * Set {@link UpiQRMethodResponse#getIdPayment} property.
          *
          * 
          */
        @NotNull Builder idPayment(IdPayment idPayment);


        /**
          * Set {@link UpiQRMethodResponse#getMoney} property.
          *
          * 
          */
        @NotNull Builder money(Money money);


        /**
          * Set {@link UpiQRMethodResponse#getVat} property.
          *
          * 
          */
        @NotNull Builder vat(MoneyVat vat);


        /**
          * Set {@link UpiQRMethodResponse#getMerchantName} property.
          *
          * 
          */
        @NotNull Builder merchantName(String merchantName);


        /**
          * Set {@link UpiQRMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @NotNull Builder reference(String reference);


        /**
          * Set {@link UpiQRMethodResponse#getQrName} property.
          *
          * The name of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code of the image can be labeled by qrName to increase the clarity of the payment instruction.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @NotNull Builder qrName(String qrName);


        /**
          * Set {@link UpiQRMethodResponse#getQrCode} property.
          *
          * The URL of the QR code image to be scanned by a wallet or payment service compatible with this payment method. The QR code encodes the instructions how make a payment.
If this parameter contains any value, include it in the payment instructions for your customer.
          */
        @NotNull Builder qrCode(String qrCode);


        /**
          * Set {@link UpiQRMethodResponse#getUpiQrDeepLink} property.
          *
          * It can be used as deep link button target (what is typically known as an intent trigger)
or to generate a QR code that can be scanned with any UPI enabled app.
          */
        @NotNull Builder upiQrDeepLink(String upiQrDeepLink);


        /**
          * Set {@link UpiQRMethodResponse#getPaymentOperator} property.
          *
          * 
          */
        @NotNull Builder paymentOperator(PaymentOperatorIncoming paymentOperator);


        /**
          * Set {@link UpiQRMethodResponse#getUpiId} property.
          *
          * Virtual payment address where we expect that your customer sends funds to make a payment. This parameter is to be shown to your customer.
          */
        @NotNull Builder upiId(String upiId);


        /**
          * Set {@link UpiQRMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @NotNull Builder returnUrl(String returnUrl);


        /**
          * Set {@link UpiQRMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @NotNull Builder acceptedAt(java.time.OffsetDateTime acceptedAt);


        /**
          * Set {@link UpiQRMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @NotNull Builder expireAt(java.time.OffsetDateTime expireAt);


        /**
         * Create new instance of {@link UpiQRMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull UpiQRMethodResponse build();

    }
}