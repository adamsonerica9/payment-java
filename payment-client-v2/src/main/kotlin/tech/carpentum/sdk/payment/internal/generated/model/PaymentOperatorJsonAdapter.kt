//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentOperator

class PaymentOperatorJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentOperatorJson): PaymentOperator {
        val builder = PaymentOperator.builder()
        builder.code(json.code)
        builder.name(json.name)
        builder.customerTransactionFee(json.customerTransactionFee)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentOperator): PaymentOperatorJson {
        val json = PaymentOperatorJson()
        json.code = model.code
        json.name = model.name
        json.customerTransactionFee = model.customerTransactionFee.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentOperator): PaymentOperatorReverseInheritanceImpl {
        return model as PaymentOperatorReverseInheritanceImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentOperatorReverseInheritanceImpl): PaymentOperator {
        return impl
    }

}