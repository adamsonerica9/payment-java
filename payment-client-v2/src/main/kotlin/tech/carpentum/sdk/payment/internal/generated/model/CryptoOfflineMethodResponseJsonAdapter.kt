//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CryptoOfflineMethodResponse

class CryptoOfflineMethodResponseJsonAdapter {
    @FromJson
    fun fromJson(json: CryptoOfflineMethodResponseJson): CryptoOfflineMethodResponse {
        val builder = CryptoOfflineMethodResponse.builder()
        builder.idPayin(json.idPayin)
        builder.idPayment(json.idPayment)
        builder.account(json.account)
        builder.money(json.money)
        builder.vat(json.vat)
        builder.merchantName(json.merchantName)
        builder.reference(json.reference)
        builder.qrName(json.qrName)
        builder.qrCode(json.qrCode)
        builder.paymentOperator(json.paymentOperator)
        builder.returnUrl(json.returnUrl)
        builder.acceptedAt(json.acceptedAt)
        builder.expireAt(json.expireAt)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CryptoOfflineMethodResponse): CryptoOfflineMethodResponseJson {
        val json = CryptoOfflineMethodResponseJson()
        json.idPayin = model.idPayin
        json.idPayment = model.idPayment
        json.account = model.account
        json.money = model.money
        json.vat = model.vat.orElse(null)
        json.merchantName = model.merchantName
        json.reference = model.reference
        json.qrName = model.qrName
        json.qrCode = model.qrCode
        json.paymentOperator = model.paymentOperator
        json.returnUrl = model.returnUrl
        json.acceptedAt = model.acceptedAt
        json.expireAt = model.expireAt
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CryptoOfflineMethodResponse): CryptoOfflineMethodResponseImpl {
        return model as CryptoOfflineMethodResponseImpl
    }

    @ToJson
    fun toJsonImpl(impl: CryptoOfflineMethodResponseImpl): CryptoOfflineMethodResponse {
        return impl
    }

}