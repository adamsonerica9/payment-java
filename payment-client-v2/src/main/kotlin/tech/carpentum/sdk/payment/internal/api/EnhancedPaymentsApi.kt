package tech.carpentum.sdk.payment.internal.api

import tech.carpentum.sdk.payment.internal.generated.api.PaymentsApi
import tech.carpentum.sdk.payment.internal.generated.infrastructure.RequestConfig
import java.time.Duration

internal class EnhancedPaymentsApi(
    basePath: String = defaultBasePath,
    private val accessToken: String,
    callTimeout: Duration
) : PaymentsApi(basePath, ApiUtils.getClient(callTimeout)) {

    override fun <T> updateAuthParams(requestConfig: RequestConfig<T>) {
        requestConfig.addAuthorizationHeader(accessToken)
    }
}
