//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.VietQRMethod

class VietQRMethodJsonAdapter {
    @FromJson
    fun fromJson(json: VietQRMethodJson): VietQRMethod {
        val builder = VietQRMethod.builder()
        builder.paymentOperatorCode(json.paymentOperatorCode)
        builder.emailAddress(json.emailAddress)
        return builder.build()
    }

    @ToJson
    fun toJson(model: VietQRMethod): VietQRMethodJson {
        val json = VietQRMethodJson()
        json.paymentOperatorCode = model.paymentOperatorCode
        json.emailAddress = model.emailAddress.orElse(null)
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: VietQRMethod): VietQRMethodImpl {
        return model as VietQRMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: VietQRMethodImpl): VietQRMethod {
        return impl
    }

}