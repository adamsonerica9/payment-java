//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestPromptPay

class AccountPayinRequestPromptPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestPromptPayJson): AccountPayinRequestPromptPay {
        val builder = AccountPayinRequestPromptPay.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestPromptPay): AccountPayinRequestPromptPayJson {
        val json = AccountPayinRequestPromptPayJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestPromptPay): AccountPayinRequestPromptPayImpl {
        return model as AccountPayinRequestPromptPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestPromptPayImpl): AccountPayinRequestPromptPay {
        return impl
    }

}