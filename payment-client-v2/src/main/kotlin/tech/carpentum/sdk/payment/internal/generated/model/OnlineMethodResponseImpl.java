//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** ONLINE
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class OnlineMethodResponseImpl implements OnlineMethodResponse {
    private final IdPayin idPayin;

    @Override
    public IdPayin getIdPayin() {
        return idPayin;
    }


    private final IdPayment idPayment;

    @Override
    public IdPayment getIdPayment() {
        return idPayment;
    }


    private final AccountResponseOnlyWithBank account;

    @Override
    public AccountResponseOnlyWithBank getAccount() {
        return account;
    }


    private final Money money;

    @Override
    public Money getMoney() {
        return money;
    }


    private final Optional<MoneyRequired> moneyRequired;

    @Override
    public Optional<MoneyRequired> getMoneyRequired() {
        return moneyRequired;
    }


    private final Optional<MoneyVat> vat;

    @Override
    public Optional<MoneyVat> getVat() {
        return vat;
    }


    /** Reference number of transaction. */
    private final String reference;

    @Override
    public String getReference() {
        return reference;
    }


    /** This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based. */
    private final String returnUrl;

    @Override
    public String getReturnUrl() {
        return returnUrl;
    }


    /** Date and time when payment was accepted. */
    private final java.time.OffsetDateTime acceptedAt;

    @Override
    public java.time.OffsetDateTime getAcceptedAt() {
        return acceptedAt;
    }


    /** Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow. */
    private final java.time.OffsetDateTime expireAt;

    @Override
    public java.time.OffsetDateTime getExpireAt() {
        return expireAt;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private OnlineMethodResponseImpl(BuilderImpl builder) {
        this.idPayin = Objects.requireNonNull(builder.idPayin, "Property 'idPayin' is required.");
        this.idPayment = Objects.requireNonNull(builder.idPayment, "Property 'idPayment' is required.");
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.moneyRequired = Optional.ofNullable(builder.moneyRequired);
        this.vat = Optional.ofNullable(builder.vat);
        this.reference = Objects.requireNonNull(builder.reference, "Property 'reference' is required.");
        this.returnUrl = Objects.requireNonNull(builder.returnUrl, "Property 'returnUrl' is required.");
        this.acceptedAt = Objects.requireNonNull(builder.acceptedAt, "Property 'acceptedAt' is required.");
        this.expireAt = Objects.requireNonNull(builder.expireAt, "Property 'expireAt' is required.");

        this.hashCode = Objects.hash(idPayin, idPayment, account, money, moneyRequired, vat, reference, returnUrl, acceptedAt, expireAt);
        this.toString = builder.type + "(" +
                "idPayin=" + idPayin +
                ", idPayment=" + idPayment +
                ", account=" + account +
                ", money=" + money +
                ", moneyRequired=" + moneyRequired +
                ", vat=" + vat +
                ", reference=" + reference +
                ", returnUrl=" + returnUrl +
                ", acceptedAt=" + acceptedAt +
                ", expireAt=" + expireAt +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof OnlineMethodResponseImpl)) {
            return false;
        }

        OnlineMethodResponseImpl that = (OnlineMethodResponseImpl) obj;
        if (!this.idPayin.equals(that.idPayin)) return false;
        if (!this.idPayment.equals(that.idPayment)) return false;
        if (!this.account.equals(that.account)) return false;
        if (!this.money.equals(that.money)) return false;
        if (!this.moneyRequired.equals(that.moneyRequired)) return false;
        if (!this.vat.equals(that.vat)) return false;
        if (!this.reference.equals(that.reference)) return false;
        if (!this.returnUrl.equals(that.returnUrl)) return false;
        if (!this.acceptedAt.equals(that.acceptedAt)) return false;
        if (!this.expireAt.equals(that.expireAt)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link OnlineMethodResponse} model class. */
    public static class BuilderImpl implements OnlineMethodResponse.Builder {
        private IdPayin idPayin = null;
        private IdPayment idPayment = null;
        private AccountResponseOnlyWithBank account = null;
        private Money money = null;
        private MoneyRequired moneyRequired = null;
        private MoneyVat vat = null;
        private String reference = null;
        private String returnUrl = null;
        private java.time.OffsetDateTime acceptedAt = null;
        private java.time.OffsetDateTime expireAt = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("OnlineMethodResponse");
        }

        /**
          * Set {@link OnlineMethodResponse#getIdPayin} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayin(IdPayin idPayin) {
            this.idPayin = idPayin;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getIdPayment} property.
          *
          * 
          */
        @Override
        public BuilderImpl idPayment(IdPayment idPayment) {
            this.idPayment = idPayment;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountResponseOnlyWithBank account) {
            this.account = account;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(Money money) {
            this.money = money;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getMoneyRequired} property.
          *
          * 
          */
        @Override
        public BuilderImpl moneyRequired(MoneyRequired moneyRequired) {
            this.moneyRequired = moneyRequired;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getVat} property.
          *
          * 
          */
        @Override
        public BuilderImpl vat(MoneyVat vat) {
            this.vat = vat;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getReference} property.
          *
          * Reference number of transaction.
          */
        @Override
        public BuilderImpl reference(String reference) {
            this.reference = reference;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getReturnUrl} property.
          *
          * This is the URL where the customers will be redirected after completing a payment.

The URL must be either IP or domain-based.
          */
        @Override
        public BuilderImpl returnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getAcceptedAt} property.
          *
          * Date and time when payment was accepted.
          */
        @Override
        public BuilderImpl acceptedAt(java.time.OffsetDateTime acceptedAt) {
            this.acceptedAt = acceptedAt;
            return this;
        }

        /**
          * Set {@link OnlineMethodResponse#getExpireAt} property.
          *
          * Date and time of payment expiration. If no money has been transferred to this time, payment is considered failed and callback with status change event will shortly follow.
          */
        @Override
        public BuilderImpl expireAt(java.time.OffsetDateTime expireAt) {
            this.expireAt = expireAt;
            return this;
        }

        /**
         * Create new instance of {@link OnlineMethodResponse} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public OnlineMethodResponseImpl build() {
            return new OnlineMethodResponseImpl(this);
        }

    }
}