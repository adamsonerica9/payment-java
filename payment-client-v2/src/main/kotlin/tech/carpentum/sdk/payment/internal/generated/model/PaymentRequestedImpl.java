//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * The requested payment.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PaymentRequestedImpl implements PaymentRequested {
    private final Money money;

    @Override
    public Money getMoney() {
        return money;
    }


    private final Optional<SegmentCode> segmentCode;

    @Override
    public Optional<SegmentCode> getSegmentCode() {
        return segmentCode;
    }


    private final Optional<CurrencyCode> exchangedToCurrency;

    @Override
    public Optional<CurrencyCode> getExchangedToCurrency() {
        return exchangedToCurrency;
    }




    private final int hashCode;
    private final String toString;

    private PaymentRequestedImpl(BuilderImpl builder) {
        this.money = Objects.requireNonNull(builder.money, "Property 'money' is required.");
        this.segmentCode = Optional.ofNullable(builder.segmentCode);
        this.exchangedToCurrency = Optional.ofNullable(builder.exchangedToCurrency);

        this.hashCode = Objects.hash(money, segmentCode, exchangedToCurrency);
        this.toString = builder.type + "(" +
                "money=" + money +
                ", segmentCode=" + segmentCode +
                ", exchangedToCurrency=" + exchangedToCurrency +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentRequestedImpl)) {
            return false;
        }

        PaymentRequestedImpl that = (PaymentRequestedImpl) obj;
        if (!this.money.equals(that.money)) return false;
        if (!this.segmentCode.equals(that.segmentCode)) return false;
        if (!this.exchangedToCurrency.equals(that.exchangedToCurrency)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentRequested} model class. */
    public static class BuilderImpl implements PaymentRequested.Builder {
        private Money money = null;
        private SegmentCode segmentCode = null;
        private CurrencyCode exchangedToCurrency = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentRequested");
        }

        /**
          * Set {@link PaymentRequested#getMoney} property.
          *
          * 
          */
        @Override
        public BuilderImpl money(Money money) {
            this.money = money;
            return this;
        }

        /**
          * Set {@link PaymentRequested#getSegmentCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl segmentCode(SegmentCode segmentCode) {
            this.segmentCode = segmentCode;
            return this;
        }

        /**
          * Set {@link PaymentRequested#getExchangedToCurrency} property.
          *
          * 
          */
        @Override
        public BuilderImpl exchangedToCurrency(CurrencyCode exchangedToCurrency) {
            this.exchangedToCurrency = exchangedToCurrency;
            return this;
        }

        /**
         * Create new instance of {@link PaymentRequested} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentRequestedImpl build() {
            return new PaymentRequestedImpl(this);
        }

    }
}