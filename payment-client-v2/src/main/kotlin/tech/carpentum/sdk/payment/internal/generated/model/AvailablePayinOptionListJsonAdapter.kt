//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AvailablePayinOptionList

class AvailablePayinOptionListJsonAdapter {
    @FromJson
    fun fromJson(json: AvailablePayinOptionListJson): AvailablePayinOptionList {
        val builder = AvailablePayinOptionList.builder()
        builder.data(json.data?.toList())
        builder.dataVariantCurrencies(json.dataVariantCurrencies?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: AvailablePayinOptionList): AvailablePayinOptionListJson {
        val json = AvailablePayinOptionListJson()
        json.data = model.data.ifEmpty { null }
        json.dataVariantCurrencies = model.dataVariantCurrencies.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AvailablePayinOptionList): AvailablePayinOptionListImpl {
        return model as AvailablePayinOptionListImpl
    }

    @ToJson
    fun toJsonImpl(impl: AvailablePayinOptionListImpl): AvailablePayinOptionList {
        return impl
    }

}