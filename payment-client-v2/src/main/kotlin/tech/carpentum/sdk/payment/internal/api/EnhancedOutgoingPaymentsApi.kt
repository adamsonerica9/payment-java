package tech.carpentum.sdk.payment.internal.api

import tech.carpentum.sdk.payment.internal.generated.api.OutgoingPaymentsApi
import tech.carpentum.sdk.payment.internal.generated.infrastructure.RequestConfig
import java.time.Duration

internal class EnhancedOutgoingPaymentsApi(
    basePath: String = defaultBasePath,
    private val accessToken: String,
    callTimeout: Duration
) : OutgoingPaymentsApi(basePath, ApiUtils.getClient(callTimeout)) {

    override fun <T> updateAuthParams(requestConfig: RequestConfig<T>) {
        requestConfig.addAuthorizationHeader(accessToken)
    }
}
