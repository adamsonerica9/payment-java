//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface PayinDetail {

    @NotNull PaymentRequested getPaymentRequested();

    @NotNull PaymentProcess getProcess();

    @NotNull MoneyFee getFee();

    @NotNull PayinMethodResponse getPaymentMethodResponse();

    @NotNull static Builder builder(PayinDetail copyOf) {
        Builder builder = builder();
        builder.paymentRequested(copyOf.getPaymentRequested());
        builder.process(copyOf.getProcess());
        builder.fee(copyOf.getFee());
        builder.paymentMethodResponse(copyOf.getPaymentMethodResponse());
        return builder;
    }

    @NotNull static Builder builder() {
        return new PayinDetailImpl.BuilderImpl();
    }

    /** Builder for {@link PayinDetail} model class. */
    interface Builder {

        /**
          * Set {@link PayinDetail#getPaymentRequested} property.
          *
          * 
          */
        @NotNull Builder paymentRequested(PaymentRequested paymentRequested);


        /**
          * Set {@link PayinDetail#getProcess} property.
          *
          * 
          */
        @NotNull Builder process(PaymentProcess process);


        /**
          * Set {@link PayinDetail#getFee} property.
          *
          * 
          */
        @NotNull Builder fee(MoneyFee fee);


        /**
          * Set {@link PayinDetail#getPaymentMethodResponse} property.
          *
          * 
          */
        @NotNull Builder paymentMethodResponse(PayinMethodResponse paymentMethodResponse);


        /**
         * Create new instance of {@link PayinDetail} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull PayinDetail build();

    }
}