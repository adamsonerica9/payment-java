//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.CapitecPayMethod

class CapitecPayMethodJsonAdapter {
    @FromJson
    fun fromJson(json: CapitecPayMethodJson): CapitecPayMethod {
        val builder = CapitecPayMethod.builder()
        builder.phoneNumber(json.phoneNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: CapitecPayMethod): CapitecPayMethodJson {
        val json = CapitecPayMethodJson()
        json.phoneNumber = model.phoneNumber
        json.paymentMethodCode = model.paymentMethodCode.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: CapitecPayMethod): CapitecPayMethodImpl {
        return model as CapitecPayMethodImpl
    }

    @ToJson
    fun toJsonImpl(impl: CapitecPayMethodImpl): CapitecPayMethod {
        return impl
    }

}