//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** EWALLET
 *
 * E-Wallet is a payment option on Indonesian market that allows customers to use major Indonesian wallets for payments. Customer is either automatically redirected to a chosen wallet with all payment details pre-populated to finalize the payment or directly receives push notification to authorize the payment in case of OVO wallet.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class EWalletMethodImpl implements EWalletMethod {
    private final AccountPayinRequestEWallet account;

    @Override
    public AccountPayinRequestEWallet getAccount() {
        return account;
    }


    /** Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins. */
    private final Optional<String> emailAddress;

    @Override
    public Optional<String> getEmailAddress() {
        return emailAddress;
    }


    /** Your customer mobile phone number in full international telephone number format, including country code.

If payment operator code is ID_OVO, then phoneNumber field is required. Otherwise, it is optional. */
    private final Optional<String> phoneNumber;

    @Override
    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }


    /** One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API. */
    private final String paymentOperatorCode;

    @Override
    public String getPaymentOperatorCode() {
        return paymentOperatorCode;
    }


    @Override public PaymentMethodCode getPaymentMethodCode() { return PAYMENT_METHOD_CODE; }

    private final int hashCode;
    private final String toString;

    private EWalletMethodImpl(BuilderImpl builder) {
        this.account = Objects.requireNonNull(builder.account, "Property 'account' is required.");
        this.emailAddress = Optional.ofNullable(builder.emailAddress);
        this.phoneNumber = Optional.ofNullable(builder.phoneNumber);
        this.paymentOperatorCode = Objects.requireNonNull(builder.paymentOperatorCode, "Property 'paymentOperatorCode' is required.");

        this.hashCode = Objects.hash(account, emailAddress, phoneNumber, paymentOperatorCode);
        this.toString = builder.type + "(" +
                "account=" + account +
                ", emailAddress=" + emailAddress +
                ", phoneNumber=" + phoneNumber +
                ", paymentOperatorCode=" + paymentOperatorCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof EWalletMethodImpl)) {
            return false;
        }

        EWalletMethodImpl that = (EWalletMethodImpl) obj;
        if (!this.account.equals(that.account)) return false;
        if (!this.emailAddress.equals(that.emailAddress)) return false;
        if (!this.phoneNumber.equals(that.phoneNumber)) return false;
        if (!this.paymentOperatorCode.equals(that.paymentOperatorCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link EWalletMethod} model class. */
    public static class BuilderImpl implements EWalletMethod.Builder {
        private AccountPayinRequestEWallet account = null;
        private String emailAddress = null;
        private String phoneNumber = null;
        private String paymentOperatorCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("EWalletMethod");
        }

        /**
          * Set {@link EWalletMethod#getAccount} property.
          *
          * 
          */
        @Override
        public BuilderImpl account(AccountPayinRequestEWallet account) {
            this.account = account;
            return this;
        }

        /**
          * Set {@link EWalletMethod#getEmailAddress} property.
          *
          * Your customer e-mail address in RFC 5322 format that is used for identification of the customer's payins.
          */
        @Override
        public BuilderImpl emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        /**
          * Set {@link EWalletMethod#getPhoneNumber} property.
          *
          * Your customer mobile phone number in full international telephone number format, including country code.

If payment operator code is ID_OVO, then phoneNumber field is required. Otherwise, it is optional.
          */
        @Override
        public BuilderImpl phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        /**
          * Set {@link EWalletMethod#getPaymentOperatorCode} property.
          *
          * One of following can serve as Payment Operator:

 * Financial or other institution (such as bank, card payment processor, ...) that manages transactions for your customers
 * Mobile wallet
 * Blockchain protocol for crypto currency payments

Customer is informed with the payment instructions where funds have to be transferred efficiently based on the selected Payment operator.
For getting list of the available payment options for payins use [POST /payins/!availablePaymentOptions](#operations-Incoming_payments-availablePaymentOptions) API, for payouts use [POST /payouts/!availablePaymentOptions](#operations-Outgoing_payments-availablePaymentOptions) API.
          */
        @Override
        public BuilderImpl paymentOperatorCode(String paymentOperatorCode) {
            this.paymentOperatorCode = paymentOperatorCode;
            return this;
        }

        /**
         * Create new instance of {@link EWalletMethod} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public EWalletMethodImpl build() {
            return new EWalletMethodImpl(this);
        }

    }
}