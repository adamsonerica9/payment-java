//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountCustomerResponsePromptPay

class AccountCustomerResponsePromptPayJsonAdapter {
    @FromJson
    fun fromJson(json: AccountCustomerResponsePromptPayJson): AccountCustomerResponsePromptPay {
        val builder = AccountCustomerResponsePromptPay.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountCustomerResponsePromptPay): AccountCustomerResponsePromptPayJson {
        val json = AccountCustomerResponsePromptPayJson()
        json.accountName = model.accountName.orElse(null)
        json.accountNumber = model.accountNumber
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountCustomerResponsePromptPay): AccountCustomerResponsePromptPayImpl {
        return model as AccountCustomerResponsePromptPayImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountCustomerResponsePromptPayImpl): AccountCustomerResponsePromptPay {
        return impl
    }

}