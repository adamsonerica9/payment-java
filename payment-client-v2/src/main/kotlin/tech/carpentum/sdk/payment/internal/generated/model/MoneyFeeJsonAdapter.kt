//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MoneyFee

class MoneyFeeJsonAdapter {
    @FromJson
    fun fromJson(json: MoneyFeeJson): MoneyFee {
        val builder = MoneyFee.builder()
        builder.amount(json.amount)
        builder.currencyCode(json.currencyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MoneyFee): MoneyFeeJson {
        val json = MoneyFeeJson()
        json.amount = model.amount
        json.currencyCode = model.currencyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MoneyFee): MoneyFeeImpl {
        return model as MoneyFeeImpl
    }

    @ToJson
    fun toJsonImpl(impl: MoneyFeeImpl): MoneyFee {
        return impl
    }

}