//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentOperatorList

class PaymentOperatorListJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentOperatorListJson): PaymentOperatorList {
        val builder = PaymentOperatorList.builder()
        builder.data(json.data?.toList())
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentOperatorList): PaymentOperatorListJson {
        val json = PaymentOperatorListJson()
        json.data = model.data.ifEmpty { null }
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentOperatorList): PaymentOperatorListImpl {
        return model as PaymentOperatorListImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentOperatorListImpl): PaymentOperatorList {
        return impl
    }

}