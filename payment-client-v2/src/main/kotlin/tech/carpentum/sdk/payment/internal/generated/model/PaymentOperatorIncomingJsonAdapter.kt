//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentOperatorIncoming

class PaymentOperatorIncomingJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentOperatorIncomingJson): PaymentOperatorIncoming {
        val builder = PaymentOperatorIncoming.builder()
        builder.code(json.code)
        builder.name(json.name)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentOperatorIncoming): PaymentOperatorIncomingJson {
        val json = PaymentOperatorIncomingJson()
        json.code = model.code
        json.name = model.name
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentOperatorIncoming): PaymentOperatorReverseInheritanceImpl {
        return model as PaymentOperatorReverseInheritanceImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentOperatorReverseInheritanceImpl): PaymentOperatorIncoming {
        return impl
    }

}