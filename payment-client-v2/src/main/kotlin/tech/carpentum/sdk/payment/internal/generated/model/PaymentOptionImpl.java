//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * 
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class PaymentOptionImpl implements PaymentOption {
    // tag::paymentTypeCodeEnum[]
    /** @see #getPaymentTypeCode */
    public static final String PAYMENT_TYPE_CODE_PAYIN = "PAYIN";
    /** @see #getPaymentTypeCode */
    public static final String PAYMENT_TYPE_CODE_PAYOUT = "PAYOUT";
    // end::paymentTypeCodeEnum[]
    /** Payment direction from the merchant's point of view. */
    private final String paymentTypeCode;

    @Override
    public String getPaymentTypeCode() {
        return paymentTypeCode;
    }


    private final PayinMethodCode paymentMethodCode;

    @Override
    public PayinMethodCode getPaymentMethodCode() {
        return paymentMethodCode;
    }


    private final CurrencyCode currencyCode;

    @Override
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }


    private final Optional<SegmentCode> segmentCode;

    @Override
    public Optional<SegmentCode> getSegmentCode() {
        return segmentCode;
    }


    private final IntervalNumberTo transactionAmountLimit;

    @Override
    public IntervalNumberTo getTransactionAmountLimit() {
        return transactionAmountLimit;
    }


    /** If set to false, the option is not currently available and must be activated in administration. */
    private final Boolean isAvailable;

    @Override
    public Boolean getIsAvailable() {
        return isAvailable;
    }


    /** Payment operators that can be used for paying via this payment option. */
    private final java.util.List<@NotNull PaymentOperatorOption> paymentOperators;

    @Override
    public java.util.List<@NotNull PaymentOperatorOption> getPaymentOperators() {
        return paymentOperators;
    }




    private final int hashCode;
    private final String toString;

    private PaymentOptionImpl(BuilderImpl builder) {
        this.paymentTypeCode = Objects.requireNonNull(builder.paymentTypeCode, "Property 'paymentTypeCode' is required.");
        this.paymentMethodCode = Objects.requireNonNull(builder.paymentMethodCode, "Property 'paymentMethodCode' is required.");
        this.currencyCode = Objects.requireNonNull(builder.currencyCode, "Property 'currencyCode' is required.");
        this.segmentCode = Optional.ofNullable(builder.segmentCode);
        this.transactionAmountLimit = Objects.requireNonNull(builder.transactionAmountLimit, "Property 'transactionAmountLimit' is required.");
        this.isAvailable = Objects.requireNonNull(builder.isAvailable, "Property 'isAvailable' is required.");
        this.paymentOperators = java.util.Collections.unmodifiableList(builder.paymentOperators);

        this.hashCode = Objects.hash(paymentTypeCode, paymentMethodCode, currencyCode, segmentCode, transactionAmountLimit, isAvailable, paymentOperators);
        this.toString = builder.type + "(" +
                "paymentTypeCode=" + paymentTypeCode +
                ", paymentMethodCode=" + paymentMethodCode +
                ", currencyCode=" + currencyCode +
                ", segmentCode=" + segmentCode +
                ", transactionAmountLimit=" + transactionAmountLimit +
                ", isAvailable=" + isAvailable +
                ", paymentOperators=" + paymentOperators +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof PaymentOptionImpl)) {
            return false;
        }

        PaymentOptionImpl that = (PaymentOptionImpl) obj;
        if (!this.paymentTypeCode.equals(that.paymentTypeCode)) return false;
        if (!this.paymentMethodCode.equals(that.paymentMethodCode)) return false;
        if (!this.currencyCode.equals(that.currencyCode)) return false;
        if (!this.segmentCode.equals(that.segmentCode)) return false;
        if (!this.transactionAmountLimit.equals(that.transactionAmountLimit)) return false;
        if (!this.isAvailable.equals(that.isAvailable)) return false;
        if (!this.paymentOperators.equals(that.paymentOperators)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link PaymentOption} model class. */
    public static class BuilderImpl implements PaymentOption.Builder {
        private String paymentTypeCode = null;
        private PayinMethodCode paymentMethodCode = null;
        private CurrencyCode currencyCode = null;
        private SegmentCode segmentCode = null;
        private IntervalNumberTo transactionAmountLimit = null;
        private Boolean isAvailable = null;
        private java.util.List<@NotNull PaymentOperatorOption> paymentOperators = new java.util.ArrayList<>();

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("PaymentOption");
        }

        /**
          * Set {@link PaymentOption#getPaymentTypeCode} property.
          *
          * Payment direction from the merchant's point of view.
          */
        @Override
        public BuilderImpl paymentTypeCode(String paymentTypeCode) {
            this.paymentTypeCode = paymentTypeCode;
            return this;
        }

        /**
          * Set {@link PaymentOption#getPaymentMethodCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl paymentMethodCode(PayinMethodCode paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
            return this;
        }

        /**
          * Set {@link PaymentOption#getCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCode(CurrencyCode currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        /**
          * Set {@link PaymentOption#getSegmentCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl segmentCode(SegmentCode segmentCode) {
            this.segmentCode = segmentCode;
            return this;
        }

        /**
          * Set {@link PaymentOption#getTransactionAmountLimit} property.
          *
          * 
          */
        @Override
        public BuilderImpl transactionAmountLimit(IntervalNumberTo transactionAmountLimit) {
            this.transactionAmountLimit = transactionAmountLimit;
            return this;
        }

        /**
          * Set {@link PaymentOption#getIsAvailable} property.
          *
          * If set to false, the option is not currently available and must be activated in administration.
          */
        @Override
        public BuilderImpl isAvailable(Boolean isAvailable) {
            this.isAvailable = isAvailable;
            return this;
        }

        /**
          * Replace all items in {@link PaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @Override
        public BuilderImpl paymentOperators(java.util.List<@NotNull PaymentOperatorOption> paymentOperators) {
            this.paymentOperators.clear();
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }
        /**
          * Add single item to {@link PaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @Override
        public BuilderImpl paymentOperatorsAdd(PaymentOperatorOption item) {
            if (item != null) {
                this.paymentOperators.add(item);
            }
            return this;
        }
        /**
          * Add all items to {@link PaymentOption#getPaymentOperators} list property.
          *
          * Payment operators that can be used for paying via this payment option.
          */
        @Override
        public BuilderImpl paymentOperatorsAddAll(java.util.List<@NotNull PaymentOperatorOption> paymentOperators) {
            if (paymentOperators != null) {
                this.paymentOperators.addAll(paymentOperators);
            }
            return this;
        }


        /**
         * Create new instance of {@link PaymentOption} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public PaymentOptionImpl build() {
            return new PaymentOptionImpl(this);
        }

    }
}