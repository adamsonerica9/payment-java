//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

/**
 * Mark interface for business validation error model classes:
 * <ul>
 *     <li>{@link PostAvailablePayoutOptionsError}</li>
 *     <li>{@link PostAvailablePayinOptionsError}</li>
 *     <li>{@link PostPayinsError}</li>
 *     <li>{@link GetPayoutError}</li>
 *     <li>{@link PostPayoutsError}</li>
 *     <li>{@link GetPaymentOptionsError}</li>
 *     <li>{@link PostAuthTokensError}</li>
 *     <li>{@link GetPayinError}</li>
 * </ul>
 */
public abstract class BusinessValidationError {
    public final @NotNull String code;

    /** Optional error description. */
    public final @NotNull Optional<String> description;

    /** Optional identification of the request attribute name that caused the error. */
    public final @NotNull Optional<String> attrCode;

    private final int hashCode;
    private final String toString;

    protected <M extends BusinessValidationError, B extends Builder<M, B>> BusinessValidationError(Builder<M, B> builder) {
        this.code = Objects.requireNonNull(builder.code, "Property 'code' is required.");
        this.description = Optional.ofNullable(builder.description);
        this.attrCode = Optional.ofNullable(builder.attrCode);

        this.hashCode = Objects.hash(code, description, attrCode);
        this.toString = getClass().getSimpleName() + "(" +
                "code=" + code +
                ", description=" + description +
                ", attrCode=" + attrCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!this.getClass().isAssignableFrom(obj.getClass())) {
            return false;
        }

        BusinessValidationError that = (BusinessValidationError) obj;
        if (!this.code.equals(that.code)) return false;
        if (!this.description.equals(that.description)) return false;
        if (!this.attrCode.equals(that.attrCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }


    public static abstract class Builder<M extends BusinessValidationError, B extends Builder<M, B>> {
        private String code = null;
        private String description = null;
        private String attrCode = null;

        protected Builder() {}

        /**
         * Set {@link BusinessValidationError#code} property.
         */
        public Builder code(String code) {
            this.code = code;
            return this;
        }

        /**
         * Set {@link BusinessValidationError#description} property.
         *
         * Optional error description.
         */
        public Builder description(String description) {
            this.description = description;
            return this;
        }

        /**
         * Set {@link BusinessValidationError#attrCode} property.
         *
         * Optional identification of the request attribute name that caused the error.
         */
        public Builder attrCode(String attrCode) {
            this.attrCode = attrCode;
            return this;
        }

        /**
         * Create new instance of model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        public abstract M build();
    }
}