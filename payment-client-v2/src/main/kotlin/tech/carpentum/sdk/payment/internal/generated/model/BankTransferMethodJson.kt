//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class BankTransferMethodJson {
    var account: AccountPayoutRequestBankTransfer? = null
    var paymentOperatorCode: String? = null
    var emailAddress: String? = null
    var remark: String? = null
    var paymentMethodCode: String? = null
}