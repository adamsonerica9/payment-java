//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PayoutMethodCode

class PayoutMethodCodeJsonAdapter {

    @FromJson
    fun fromJson(json: String): PayoutMethodCode {
        return PayoutMethodCode.of(json)
    }

    @ToJson
    fun toJson(model: PayoutMethodCode): String {
        return model.value
    }

    @FromJson
    fun fromJsonImpl(model: PayoutMethodCode): PayoutMethodCodeImpl {
        return model as PayoutMethodCodeImpl
    }

    @ToJson
    fun toJsonImpl(impl: PayoutMethodCodeImpl): PayoutMethodCode {
        return impl
    }

}