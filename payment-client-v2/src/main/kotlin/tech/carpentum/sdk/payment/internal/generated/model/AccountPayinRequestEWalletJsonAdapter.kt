//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountPayinRequestEWallet

class AccountPayinRequestEWalletJsonAdapter {
    @FromJson
    fun fromJson(json: AccountPayinRequestEWalletJson): AccountPayinRequestEWallet {
        val builder = AccountPayinRequestEWallet.builder()
        builder.accountName(json.accountName)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountPayinRequestEWallet): AccountPayinRequestEWalletJson {
        val json = AccountPayinRequestEWalletJson()
        json.accountName = model.accountName.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountPayinRequestEWallet): AccountPayinRequestEWalletImpl {
        return model as AccountPayinRequestEWalletImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountPayinRequestEWalletImpl): AccountPayinRequestEWallet {
        return impl
    }

}