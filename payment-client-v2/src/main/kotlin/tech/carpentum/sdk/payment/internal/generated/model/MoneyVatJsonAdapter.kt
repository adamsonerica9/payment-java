//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.MoneyVat

class MoneyVatJsonAdapter {
    @FromJson
    fun fromJson(json: MoneyVatJson): MoneyVat {
        val builder = MoneyVat.builder()
        builder.amount(json.amount)
        builder.currencyCode(json.currencyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: MoneyVat): MoneyVatJson {
        val json = MoneyVatJson()
        json.amount = model.amount
        json.currencyCode = model.currencyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: MoneyVat): MoneyVatImpl {
        return model as MoneyVatImpl
    }

    @ToJson
    fun toJsonImpl(impl: MoneyVatImpl): MoneyVat {
        return impl
    }

}