package tech.carpentum.sdk.payment.internal.api

import tech.carpentum.sdk.payment.internal.generated.api.MerchantInfoApi
import tech.carpentum.sdk.payment.internal.generated.infrastructure.RequestConfig
import java.time.Duration

internal class EnhancedMerchantInfoApi(
    basePath: String = defaultBasePath,
    private val accessToken: String,
    callTimeout: Duration
) : MerchantInfoApi(basePath, ApiUtils.getClient(callTimeout)) {

    override fun <T> updateAuthParams(requestConfig: RequestConfig<T>) {
        requestConfig.addAuthorizationHeader(accessToken)
    }
}
