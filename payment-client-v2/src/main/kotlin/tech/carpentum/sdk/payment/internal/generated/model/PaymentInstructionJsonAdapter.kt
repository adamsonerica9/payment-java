//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.PaymentInstruction

class PaymentInstructionJsonAdapter {
    @FromJson
    fun fromJson(json: PaymentInstructionJson): PaymentInstruction {
        val builder = PaymentInstruction.builder()
        builder.bankingService(json.bankingService)
        builder.companyCode(json.companyCode)
        return builder.build()
    }

    @ToJson
    fun toJson(model: PaymentInstruction): PaymentInstructionJson {
        val json = PaymentInstructionJson()
        json.bankingService = model.bankingService
        json.companyCode = model.companyCode
        return json
    }

    @FromJson
    fun fromJsonImpl(model: PaymentInstruction): PaymentInstructionImpl {
        return model as PaymentInstructionImpl
    }

    @ToJson
    fun toJsonImpl(impl: PaymentInstructionImpl): PaymentInstruction {
        return impl
    }

}