//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * Amount of Value-Added Tax is returned in case it's levied on a payment order.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public class MoneyVatImpl implements MoneyVat {
    /** Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34).
The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted). */
    private final java.math.BigDecimal amount;

    @Override
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    private final CurrencyCode currencyCode;

    @Override
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }




    private final int hashCode;
    private final String toString;

    private MoneyVatImpl(BuilderImpl builder) {
        this.amount = Objects.requireNonNull(builder.amount, "Property 'amount' is required.");
        this.currencyCode = Objects.requireNonNull(builder.currencyCode, "Property 'currencyCode' is required.");

        this.hashCode = Objects.hash(amount, currencyCode);
        this.toString = builder.type + "(" +
                "amount=" + amount +
                ", currencyCode=" + currencyCode +
                ')';
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof MoneyVatImpl)) {
            return false;
        }

        MoneyVatImpl that = (MoneyVatImpl) obj;
        if (!this.amount.equals(that.amount)) return false;
        if (!this.currencyCode.equals(that.currencyCode)) return false;

        return true;
    }

    @Override
    public String toString() {
        return toString;
    }

    /** Builder for {@link MoneyVat} model class. */
    public static class BuilderImpl implements MoneyVat.Builder {
        private java.math.BigDecimal amount = null;
        private CurrencyCode currencyCode = null;

        private final String type;

        public BuilderImpl(String type) {
            this.type = type;
        }

        public BuilderImpl() {
            this("MoneyVat");
        }

        /**
          * Set {@link MoneyVat#getAmount} property.
          *
          * Amount is accepted in the smallest currency unit. For fiat currencies the smallest currency unit are based on ISO 4217 (e.g. for USD two decimal places are available so amount can be accepted as 12.34).
The exceptions are IDR and INR currencies: zero amount in decimal places are allowed for them (e.g. 42.05 is not allowed, while 42 or 42.00 or 43 or 43.00 are allowed).

If more decimal places than supported is provided request will fail on HTTP 406 error (e.g. for USD the amount 12.345 will not be accepted).
          */
        @Override
        public BuilderImpl amount(java.math.BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        /**
          * Set {@link MoneyVat#getCurrencyCode} property.
          *
          * 
          */
        @Override
        public BuilderImpl currencyCode(CurrencyCode currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        /**
         * Create new instance of {@link MoneyVat} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @Override
        public MoneyVatImpl build() {
            return new MoneyVatImpl(this);
        }

    }
}