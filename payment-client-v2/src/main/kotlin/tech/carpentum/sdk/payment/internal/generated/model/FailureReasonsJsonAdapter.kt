//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.FailureReasons

class FailureReasonsJsonAdapter {
    @FromJson
    fun fromJson(json: FailureReasonsJson): FailureReasons {
        val builder = FailureReasons.builder()
        builder.failedAt(json.failedAt)
        builder.failureReasonCode(json.failureReasonCode)
        builder.message(json.message)
        return builder.build()
    }

    @ToJson
    fun toJson(model: FailureReasons): FailureReasonsJson {
        val json = FailureReasonsJson()
        json.failedAt = model.failedAt
        json.failureReasonCode = model.failureReasonCode
        json.message = model.message.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: FailureReasons): FailureReasonsImpl {
        return model as FailureReasonsImpl
    }

    @ToJson
    fun toJsonImpl(impl: FailureReasonsImpl): FailureReasons {
        return impl
    }

}