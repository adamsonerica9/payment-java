//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * Payment method is a payment instrument that your customers use (such as online bank transfers, VA payments, UPI and so on).
Select a payment method from the list of the methods that you have configured in your account.

The `paymentMethodCode` represents payment method available for payin payment.
 *
 * @see UpiQRMethod
 * @see CryptoOfflineMethod
 * @see VietQRMethod
 * @see NetBankingMethod
 * @see P2AV2Method
 * @see DuitNowMethod
 * @see UpiIdMethod
 * @see CapitecPayMethod
 * @see PromptPayMethod
 * @see QrPhMethod
 * @see OnlineMethod
 * @see IMPSMethod
 * @see VaPayMethod
 * @see OfflineMethod
 * @see EWalletMethod
 * @see QrisPayMethod
 *
 * The model class is immutable.
 *
 *
 */
public interface PayinMethod {
    /** Name of discriminator property used to distinguish between "one-of" subclasses. */
    String DISCRIMINATOR = "paymentMethodCode";

    /**
     * @see PaymentMethodCode
     */
    @NotNull PaymentMethodCode getPaymentMethodCode();

    /**
     * Enumeration of "one-of" subclasses distinguished by {@code paymentMethodCode} discriminator value.
     */
    enum PaymentMethodCode {
        CAPITEC_PAY,
        CRYPTO_OFFLINE,
        DUITNOW,
        EWALLET,
        IMPS,
        NETBANKING,
        OFFLINE,
        ONLINE,
        P2A_V2,
        PROMPTPAY,
        QRISPAY,
        QRPH,
        UPIID,
        UPIQR,
        VAPAY,
        VIETQR,
    }
}