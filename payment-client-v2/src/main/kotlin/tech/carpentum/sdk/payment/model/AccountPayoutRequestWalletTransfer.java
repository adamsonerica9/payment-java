//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.model;

import com.squareup.moshi.JsonClass;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import tech.carpentum.sdk.payment.internal.generated.model.*;
import tech.carpentum.sdk.payment.model.*;

/** 
 *
 * Parameters of a customer's wallet information where your customer would like his funds to be transferred.
 *
 * 
 *
 * The model class is immutable.
 * Use static {@link #builder} method to create a new {@link Builder} instance to build the model class instance.
 *
 */
@JsonClass(generateAdapter = false)
public interface AccountPayoutRequestWalletTransfer {

    /** Name of a person who holds a wallet where your customer would like his funds to be transferred.

If currency is INR, then accountName field is required. Otherwise, it is optional. */
    @NotNull Optional<String> getAccountName();

    /** Address of customer's wallet where your customer would like his funds to be transferred. */
    @NotNull String getAccountNumber();

    @NotNull static Builder builder(AccountPayoutRequestWalletTransfer copyOf) {
        Builder builder = builder();
        builder.accountName(copyOf.getAccountName().orElse(null));
        builder.accountNumber(copyOf.getAccountNumber());
        return builder;
    }

    @NotNull static Builder builder() {
        return new AccountPayoutRequestWalletTransferImpl.BuilderImpl();
    }

    /** Builder for {@link AccountPayoutRequestWalletTransfer} model class. */
    interface Builder {

        /**
          * Set {@link AccountPayoutRequestWalletTransfer#getAccountName} property.
          *
          * Name of a person who holds a wallet where your customer would like his funds to be transferred.

If currency is INR, then accountName field is required. Otherwise, it is optional.
          */
        @NotNull Builder accountName(String accountName);


        /**
          * Set {@link AccountPayoutRequestWalletTransfer#getAccountNumber} property.
          *
          * Address of customer's wallet where your customer would like his funds to be transferred.
          */
        @NotNull Builder accountNumber(String accountNumber);


        /**
         * Create new instance of {@link AccountPayoutRequestWalletTransfer} model class with the builder instance properties.
         *
         * @throws NullPointerException in case required properties are not specified.
         */
        @NotNull AccountPayoutRequestWalletTransfer build();

    }
}