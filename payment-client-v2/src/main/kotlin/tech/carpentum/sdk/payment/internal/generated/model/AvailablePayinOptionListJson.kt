//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.JsonClass;
import tech.carpentum.sdk.payment.model.*;

@JsonClass(generateAdapter = true)
class AvailablePayinOptionListJson {
    var data: List<AvailablePayinOption>? = null
    var dataVariantCurrencies: List<AvailablePayinOptionVariantCurrency>? = null
}