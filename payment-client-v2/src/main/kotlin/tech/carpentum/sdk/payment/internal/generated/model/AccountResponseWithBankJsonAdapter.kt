//THE FILE IS GENERATED, DO NOT MODIFY IT MANUALLY!!!
package tech.carpentum.sdk.payment.internal.generated.model;

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import tech.carpentum.sdk.payment.model.AccountResponseWithBank

class AccountResponseWithBankJsonAdapter {
    @FromJson
    fun fromJson(json: AccountResponseWithBankJson): AccountResponseWithBank {
        val builder = AccountResponseWithBank.builder()
        builder.accountName(json.accountName)
        builder.accountNumber(json.accountNumber)
        builder.bankCode(json.bankCode)
        builder.bankName(json.bankName)
        builder.bankBranch(json.bankBranch)
        builder.bankCity(json.bankCity)
        builder.bankProvince(json.bankProvince)
        return builder.build()
    }

    @ToJson
    fun toJson(model: AccountResponseWithBank): AccountResponseWithBankJson {
        val json = AccountResponseWithBankJson()
        json.accountName = model.accountName
        json.accountNumber = model.accountNumber
        json.bankCode = model.bankCode.orElse(null)
        json.bankName = model.bankName.orElse(null)
        json.bankBranch = model.bankBranch.orElse(null)
        json.bankCity = model.bankCity.orElse(null)
        json.bankProvince = model.bankProvince.orElse(null)
        return json
    }

    @FromJson
    fun fromJsonImpl(model: AccountResponseWithBank): AccountResponseOfflineImpl {
        return model as AccountResponseOfflineImpl
    }

    @ToJson
    fun toJsonImpl(impl: AccountResponseOfflineImpl): AccountResponseWithBank {
        return impl
    }

}