# Module Carpentum Payment system Java SDK

Start with [tech.carpentum.sdk.payment.PaymentContext] class -
visit [User Guide](https://gitlab.com/carpentumpublic/sdk/payment-java/-/blob/master/documentation/user-guide.adoc)
for more details about the API usage.

# Package tech.carpentum.sdk.payment

The root package of the SDK - start with [tech.carpentum.sdk.payment.PaymentContext] class.

# Package tech.carpentum.sdk.payment.model

The package contains model classes used as an HTTP request and response bodies.
