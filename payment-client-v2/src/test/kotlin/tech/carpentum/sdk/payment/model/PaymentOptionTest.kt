package tech.carpentum.sdk.payment.model

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import java.math.BigDecimal

/**
 * Tests [PaymentOption].
 */
class PaymentOptionTest : FunSpec({

    test("Property paymentMethodCode should be String - compilation test") {
        PaymentOption.builder()
            .paymentTypeCode(PaymentOption.PAYMENT_TYPE_CODE_PAYIN)
            .paymentMethodCode(PayinMethodCode.of("payment1"))
            .currencyCode(CurrencyCode.of("currency1"))
            .segmentCode(SegmentCode.of("segment1"))
            .transactionAmountLimit(IntervalNumberTo.builder().from(BigDecimal(1)).to(BigDecimal(100)).build())
            .isAvailable(true)
            .paymentOperatorsAdd(
                PaymentOperatorOption.builder()
                    .code("operator1")
                    .name("Operator 1")
                    .isAvailable(true)
                    .transactionAmountLimit(IntervalNumberTo.builder().from(BigDecimal(10)).to(BigDecimal(60)).build())
                    .build()
            )
            .paymentOperatorsAdd(
                PaymentOperatorOption.builder()
                    .code("operator2")
                    .name("Operator 2")
                    .isAvailable(true)
                    .transactionAmountLimit(IntervalNumberTo.builder().from(BigDecimal(40)).to(BigDecimal(90)).build())
                    .build()
            )
            .build()
            .toString() shouldBe "PaymentOption(paymentTypeCode=PAYIN, paymentMethodCode=payment1, currencyCode=currency1, segmentCode=Optional[segment1], transactionAmountLimit=IntervalNumberTo(from=Optional[1], to=Optional[100]), isAvailable=true, paymentOperators=[PaymentOperatorOption(code=operator1, name=Optional[Operator 1], isAvailable=true, transactionAmountLimit=IntervalNumberTo(from=Optional[10], to=Optional[60])), PaymentOperatorOption(code=operator2, name=Optional[Operator 2], isAvailable=true, transactionAmountLimit=IntervalNumberTo(from=Optional[40], to=Optional[90]))])"
    }

})
