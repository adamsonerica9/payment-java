package tech.carpentum.sdk.payment

object CompilationTestSupport {
    val context = PaymentContext.builder().build()
    val token = context.createAuthToken(AuthTokenOperations()).token

    val accountsApi: AccountsApi = AccountsApi.Factory.create(context, token)
    val incomingPaymentsApi: IncomingPaymentsApi = IncomingPaymentsApi.Factory.create(context, token)
    val outgoingPaymentsApi: OutgoingPaymentsApi = OutgoingPaymentsApi.Factory.create(context, token)
    val paymentsApi: PaymentsApi = PaymentsApi.Factory.create(context, token)
    val merchantInfoApi: MerchantInfoApi = MerchantInfoApi.Factory.create(context, token)
}
