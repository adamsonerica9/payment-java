package tech.carpentum.sdk.payment.model

import tech.carpentum.sdk.payment.CompilationTestSupport.incomingPaymentsApi
import tech.carpentum.sdk.payment.CompilationTestSupport.outgoingPaymentsApi
import tech.carpentum.sdk.payment.CompilationTestSupport.paymentsApi
import java.util.*

/**
 * Tests backward compatible changes in [PaymentOperator].
 */
object PaymentOperatorCompilationTest {

    fun compilationTest() {
        // PaymentOperatorList
        val listPaymentOperators: PaymentOperatorList = paymentsApi.listPaymentOperators()
        listPaymentOperators.data.forEach { paymentOperator: PaymentOperator -> println("${paymentOperator.code} - ${paymentOperator.name}") }
        // AvailablePayinOption
        val availablePayinOptions: AvailablePayinOptionList = incomingPaymentsApi.availablePaymentOptions(PaymentRequested.builder().build())
        availablePayinOptions.data.forEach { availablePayinOption: AvailablePayinOption ->
            availablePayinOption.paymentOperators.forEach { paymentOperator: PaymentOperator -> println(paymentOperator) }
        }
        // AvailablePayoutOption
        val availablePayoutOptions: AvailablePayoutOptionList = outgoingPaymentsApi.availablePaymentOptions(PaymentRequested.builder().build())
        availablePayoutOptions.data.forEach { availablePayoutOption: AvailablePayoutOption ->
            availablePayoutOption.paymentOperators.forEach { paymentOperator: PaymentOperator -> println(paymentOperator) }
        }
        // DuitNowPayMethodResponse
        val duitNowPayMethodResponse: DuitNowPayMethodResponse = DuitNowPayMethodResponse.builder().build()
        val duitNowPaymentOperator: Optional<out PaymentOperator> = duitNowPayMethodResponse.paymentOperator
    }

    fun availablePayinOptionBuilder() {
        // PaymentOperator
        val availablePayinOption: AvailablePayinOption = AvailablePayinOption.builder()
            .paymentMethodCode(PayinMethodCode.of("payment1"))
            .paymentOperatorsAdd(
                PaymentOperator.builder()
                    .code("operation1")
                    .name("Operation 1")
                    .build()
            )
            .paymentOperatorsAdd(
                PaymentOperator.builder()
                    .code("operation2")
                    .name("Operation 2")
                    .build()
            )
            .segmentCode(SegmentCode.of("segment1"))
            .build()
    }

}
