plugins {
    id("org.openapi.generator") version "5.4.0"
}

openApiGenerate {
    generatorName.set("kotlin")
    inputSpec.set("$projectDir/src/openapi/openapi.yaml")
    outputDir.set("$rootDir/payment-client-v2")
    packageName.set("tech.carpentum.sdk.payment.internal.generated")
    apiPackage.set("tech.carpentum.sdk.payment.internal.generated.api")
    modelPackage.set("tech.carpentum.sdk.payment.model")
    generateApiDocumentation.set(false)
    additionalProperties.set(
        mapOf(
            "enumPropertyNaming" to "UPPERCASE",
            "moshiCodeGen" to "true",
            "sortModelPropertiesByRequiredFlag" to "false",
            "sortParamsByRequiredFlag" to "false",
        )
    )
}
