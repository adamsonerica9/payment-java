#!/usr/bin/env bash

DEVELOPER_PORTAL=developers.carpentum.tech
#DEVELOPER_PORTAL=developers.echelonpay.com

SOURCE_DIR="$(dirname $BASH_SOURCE)"

source $SOURCE_DIR/common.sh

wgetFiles() {
  cd $OPENAPI_DIR

  echo "*** $PWD ***"

  wget https://$DEVELOPER_PORTAL/api/_static/openapi.yaml \
    --output-document=openapi.yaml \
    --quiet

  ls -la *.yaml
}

wgetFiles
